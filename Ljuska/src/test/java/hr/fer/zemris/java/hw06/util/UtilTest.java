package hr.fer.zemris.java.hw06.util;

import static org.junit.jupiter.api.Assertions.*; 
import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw06.util.Util;

class UtilTest {

	@Test
	void hextobyteTest() {
		String hex1;
		hex1 = "0";
		assertThrows(IllegalArgumentException.class, () -> Util.hextobyte(hex1));
		
		String  hex2;
		hex2 = "zz";
		assertThrows(IllegalArgumentException.class, () -> Util.hextobyte(hex2));

		
		byte []testBytes = new byte[3];
		testBytes = Util.hextobyte("01aE22");
		assertEquals(1, testBytes[0]);
		assertEquals(-82, testBytes[1]);
		assertEquals(34, testBytes[2]);
		
		testBytes = Util.hextobyte("55FF80");
		assertEquals(85, testBytes[0]);
		assertEquals(-1, testBytes[1]);
		assertEquals(-128, testBytes[2]);
		
		testBytes = Util.hextobyte("");
		assertEquals(0, testBytes.length);
	}
	
	@Test
	void bytetohexTest() {
		byte []testBytes = {1, -82, 34};
		
		assertEquals("01ae22", Util.bytetohex(testBytes));
		
		byte []testBytes2 = {85, -1, -128};
		
		assertEquals("55ff80", Util.bytetohex(testBytes2));
		
		byte []testBytes3 = {};
		
		assertEquals("", Util.bytetohex(testBytes3));
	}

}
