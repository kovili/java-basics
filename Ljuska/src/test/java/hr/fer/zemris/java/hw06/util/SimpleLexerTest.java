package hr.fer.zemris.java.hw06.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SimpleLexerTest {

	@Test
	void testNullPointer() {
		assertThrows(NullPointerException.class, () -> new SimpleLexer(null));
	}
	
	@Test
	void testBlank() {
		assertEquals(null, new SimpleLexer(" \t \n \n").nextToken());
	}
	
	@Test
	void testIncorrectStringArgument() {
		assertThrows(LexerException.class, () -> new SimpleLexer("\"foobar").nextToken());
	}
	
	@Test
	void testIncorrectStringEndingArgument() {
		SimpleLexer lexer = new SimpleLexer("\"foobar\"foo");
		lexer.nextToken();
		assertThrows(LexerException.class, () -> lexer.nextToken());
	}
	
	@Test
	void testSimpleArgument() {
		SimpleLexer lexer = new SimpleLexer("foo \t\t");
		assertEquals("foo", lexer.nextToken());
		assertEquals(null, lexer.nextToken());
	}
	
	@Test
	void testSimpleStringArgument() {
		SimpleLexer lexer = new SimpleLexer(" \"foobar\" ");
		assertEquals("foobar", lexer.nextToken());
		assertEquals(null, lexer.nextToken());
	}
	
	@Test
	void testStringAndNoString() {
		SimpleLexer lexer = new SimpleLexer("foo \"foobar\" bar");
		assertEquals("foo", lexer.nextToken());
		assertEquals("foobar", lexer.nextToken());
		assertEquals("bar", lexer.nextToken());
		assertEquals(null, lexer.nextToken());
	}
	
	@Test
	void testTwoStrings() {
		SimpleLexer lexer = new SimpleLexer("\"foobar\" \"bar\"");
		assertEquals("foobar", lexer.nextToken());
		assertEquals("bar", lexer.nextToken());
		assertEquals(null, lexer.nextToken());
	}
	
	@Test
	void testSimpleEscaping() {
		SimpleLexer lexer = new SimpleLexer("\"\\\" \\\\ \\\\\\\"foobar\\f\\o\\\\b\\a\\r\"");
		assertEquals("\" \\ \\\"foobar\\f\\o\\b\\a\\r", lexer.nextToken());
		assertEquals(null, lexer.nextToken());
	}
	
	@Test
	void testComplexExample() {
		SimpleLexer lexer = new SimpleLexer("\t\t \"foo\\bar\\Korisnik\" drugiargument \"\\\"\\\\\"");
		assertEquals("foo\\bar\\Korisnik", lexer.nextToken());
		assertEquals("drugiargument", lexer.nextToken());
		assertEquals("\"\\", lexer.nextToken());
		assertEquals(null, lexer.nextToken());
	}
	
	@Test
	void simpleTokenizationTest() {
		String []tokenized = SimpleLexer.simpleTokenization("    ovo je jednostavna \tnaredba");
		assertEquals("ovo", tokenized[0]);
		assertEquals("je", tokenized[1]);
		assertEquals("jednostavna", tokenized[2]);
		assertEquals("naredba", tokenized[3]);
	}
	

}
