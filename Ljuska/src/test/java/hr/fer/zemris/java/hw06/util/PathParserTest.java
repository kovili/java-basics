package hr.fer.zemris.java.hw06.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class PathParserTest {

	@Test
	void test() {
		assertThrows(NullPointerException.class, () -> new PathParser(null));
	}
	
	@Test
	void properTokenizationTest() {
		PathParser parser = new PathParser("\t\t \"foo\\bar\\Korisnik\" drugiargument \"\\\"\\\\\"");
		assertEquals("foo\\bar\\Korisnik", parser.getTokens()[0]);
		assertEquals("drugiargument", parser.getTokens()[1]);
		assertEquals("\"\\", parser.getTokens()[2]);
	}

}
