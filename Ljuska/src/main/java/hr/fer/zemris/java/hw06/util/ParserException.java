package hr.fer.zemris.java.hw06.util;

/**
 * Iznimka koja se baca kada Parser naiđe na pogrešku u parsiranju.
 * 
 * @author Petar Kovač
 */
public class ParserException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ParserException() {
	}

	public ParserException(String message) {
		super(message);
	}
	
	public ParserException(Throwable cause) {
		super(cause);
	}
	
	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}
}
