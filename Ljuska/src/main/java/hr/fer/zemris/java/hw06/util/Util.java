package hr.fer.zemris.java.hw06.util;

import java.util.Objects;
/**
 * Klasa koja sadrži metode za pretvorbu binarnih vrijednosti u heksadecimalne brojeve i obrnuto.
 *
 * @author Petar Kovač
 *
 */
public class Util {
	//Konstanta kojim se množi prvi nibble jednog bajta da se dobiju gornja 4 bita bajta.
	private static final int HEX_MULTIPLIER = 16;
	//ASCII vrijednost nule.
	private static final int ASCII_ZERO = 48;

	
	/**
	 * Metoda koja pretvara heksadecimalni broj u polje bajtova.
	 * 
	 * @param keyText Tekstualni prikaz heksadecimalnog broja.
	 * @return  Polje bajtova koji predstavljaju ulazni heksidecimalni broj.
	 * @throws IllegalArgumentException ako heksidecimalni nema parni broj znamenki ili ako broj nije heksidecimalni.
	 * @throws NullPointerException ako je predan argument <code>null</code>.
	 */
	public static byte[] hextobyte(String keyText) {
		Objects.requireNonNull(keyText);
		if(keyText.length() % 2 != 0) {
			throw new IllegalArgumentException("Keytext must have even number of letters");
		}
		byte []result = new byte[keyText.length()/2];
		for(int i = 0, limit = keyText.length() / 2; i < limit; i++) {
			int hex1 = convertToByte(keyText.charAt(2 * i));
			int hex2 = convertToByte(keyText.charAt(2 * i + 1));
			if(hex1 == -1 || hex2 == -1) {
				throw new IllegalArgumentException("Keytext contains non-hex characters");
			}
			result[i] =   (byte) (hex1 * HEX_MULTIPLIER + hex2);
		}
		return result;
	}
	
	/**
	 * Metoda koja pretvara polje bajtova u heksidecimalni broj.
	 * 
	 * @param byteArray Ulazno polje bajtova.
	 * @return String heksadecimalni broj koji odgovara vrijednostima polja bajtova.
	 */
	public static String bytetohex(byte ... byteArray) {
		StringBuilder builder = new StringBuilder();
		for(int i = 0, limit = byteArray.length; i < limit; i++) {
			String hex1 = convertToString((byteArray[i] >> 4) & 0x0F);
			String hex2 = convertToString(byteArray[i] & 0x0F);
			builder.append(hex1);
			builder.append(hex2);
		}
		return builder.toString();
	}
	
	/**
	 * Pomoćna metoda koja pretvara jedan znak heksadecimalnog broja pretvara u odgovarajuću byte vrijednost.
	 * @param hex Heksadecimalni znak.
	 * @return Vrijednost heksadecimalnog znaka, -1 ako broj nije heksadecimalan.
	 */
	private static int convertToByte(char hex) {
		if(Character.isDigit(hex)) {
			return (int) hex % ASCII_ZERO;
		}
		switch(Character.toLowerCase(hex)) {
			case('a'):
				return 10;
			case('b'):
				return 11;
			case('c'):
				return 12;
			case('d'):
				return 13;
			case('e'):
				return 14;
			case('f'):
				return 15;
		}
		return -1;
	}
	
	/**
	 * Metoda koja pretvara jedan nibble u odgovarajuću heksadecimalnu vrijednost.
	 * 
	 * @param nibble Jedan 'nibble' jednog 'byte-a'.
	 * @return Heksadecimalni prikaz jednog 'nibble-a', prazan String, ako 
	 */
	private static String convertToString(int nibble) {
		if(nibble >= 0 && nibble < 10) {
			return Integer.toString(nibble);
		}
		switch(nibble) {
			case(10):
				return "a";
			case(11):
				return "b";
			case(12):
				return "c";
			case(13):
				return "d";
			case(14):
				return "e";
			case(15):
				return "f";
		}
		return "";
	}
	
	
}
