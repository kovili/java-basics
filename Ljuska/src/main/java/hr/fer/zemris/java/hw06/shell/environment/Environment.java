package hr.fer.zemris.java.hw06.shell.environment;

import java.util.SortedMap;

import hr.fer.zemris.java.hw06.shell.ShellIOException;
import hr.fer.zemris.java.hw06.shell.commands.ShellCommand;
/**
 * Apstraktna strategija. Modelira okruženje u kojem se izvode naredbe i 
 * definira način komunikacije s korisnikom.
 *
 * @author Petar Kovač
 */
public interface Environment {
	
	/**
	 * Čita jednu unešenu liniju.
	 * 
	 * @return Unešena linija.
	 * @throws ShellIOException Ako je došlo do greške tijekom čitanja.
	 */
	String readLine() throws ShellIOException;
	
	/**
	 * Ispisuje dani tekst korisniku.
	 * 
	 * @param text Tekst za ispisivanje.
	 * @throws ShellIOException Ako je došlo do greške tijekom ispisa.
	 */
	void write(String text) throws ShellIOException;
	
	/**
	 * Ispisuje jednu liniju teksta korisniku.
	 * 
	 * @param text Tekst za ispisivanje.
	 * @throws ShellIOException Ako je došlo do greške tijekom ispisa.
	 */
	void writeln(String text) throws ShellIOException;
	
	/**
	 * Vraća mapu svih naredbi s njihovim imenom.
	 * 
	 * @return Nepromjenjiva mapa naredbi.
	 */
	SortedMap<String, ShellCommand> commands();
	
	/**
	 * Vraća simbol koji se ispisuje svaki put kada se naredba proteže u novi redak.
	 * 
	 * @return Multiline simbol.
	 */
	Character getMultilineSymbol();
	
	/**
	 * Postavlja simbol koji se ispisuje svaki put kada se naredba proteže u novi redak na neku novu vrijednost.
	 * 
	 * @param symbol Novi multiline simbol.
	 */
	void setMultilineSymbol(Character symbol);
	/**
	 * Vraća simbol koji korisniku javlja da se nova linija očekuje.
	 * 
	 * @return Prompt simbol.
	 */
	Character getPromptSymbol();
	
	/**
	 * Postavlja simbol koji korisniku javlja da se nova linija očekuje na neku novu vrijednost.
	 * 
	 * @param symbol Novi prompt simbol.
	 */
	void setPromptSymbol(Character symbol);
	/**
	 * Vraća simbol koji se koristi kao signal da više linija instrukcija slijedi.
	 * 
	 * @return Moreline simbol.
	 */
	Character getMorelinesSymbol();
	/**
	 * Postavlja simbol koji se koristi kao signal da više linija instrukcija slijedi na neku novu vrijednost.
	 * 
	 * @param symbol Novi moreline simbol.
	 */
	void setMorelinesSymbol(Character symbol);
}
