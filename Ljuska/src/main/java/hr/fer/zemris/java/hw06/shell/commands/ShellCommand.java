package hr.fer.zemris.java.hw06.shell.commands;

import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;

/**
 * Sučelje koje modelira ponašanje jedne Shell naredbe.
 *
 * @author Petar Kovač
 */
public interface ShellCommand {
	
	/**
	 * Metoda koja izvršava naredbu opisanu implementacijom ovog sučelja.
	 * 
	 * @param env Konkretna strategija kojom se vrši komunikacija s korisnikom.
	 * @param arguments Argumenti naredbe
	 * @return ShellStatus.CONTINUE ako se naredba uspješno izvrši, 
	 * 		   ShellStatus.TERMINATE ako dođe do greške tijekom rada koja se ne može ispraviti.
	 */
	ShellStatus executeCommand(Environment env, String arguments);
	
	/**
	 * Vraća ime naredbe.
	 * 
	 * @return Ime naredbe.
	 */
	String getCommandName();
	
	/**
	 * Instrukcije za korištenje ove naredbe.
	 * 
	 * @return List<String> instrukcije.
	 */
	List<String> getCommandDescription();
	
}
