package hr.fer.zemris.java.hw06.shell;

/**
 * Enumeracija koja prikazuje dopuštena stanja koje svaka naredba vraća pri završetku naredbe.
 *
 * @author Petar Kovač
 */
public enum ShellStatus {
	/**
	 * Označuje normalan nastavak Shell programa komandne linije.
	 */
	CONTINUE,
	/**
	 * Dobivanjem ovog signala, Shell komandna linija zaustavlja s radom.
	 */
	TERMINATE
}
