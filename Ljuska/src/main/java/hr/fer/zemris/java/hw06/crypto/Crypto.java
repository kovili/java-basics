package hr.fer.zemris.java.hw06.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import hr.fer.zemris.java.hw06.util.Util;

/**
 * Program koji je sposoban izračunati sha-256 digest neke datoteke i kriptirati/dekriptirati datoteke.
 * Prvi argument mora biti sha ili encrypt ili decrypt.
 * Drugi argument mora biti izvorni dokument.
 * Treći argument je dokument u koja se upisuju kriptirani/dekriptirani podatci, koristi se
 * samo u slučaju argumenta encrypt i decrypt.
 * 
 * Ako je odabran sha, program će izračunati sha i pitati korisnika da usporedi svoj sha s izračunatim.
 * Ako je odabran encrypt, program će pomoću dodatno dane 
 * šifre i vektora slati kriptirane podatke u odredišnu datoteku.
 * Ako je odabran decrypt, program će pomoću dodatno dane
 * šifre i vektoea slati dekriptirane podatke u odredišnu datoteku.
 * 
 * Kriptiranje je simetrično, kriptirana datoteka se može s istim ključem i vektorom dekriptirati
 * u originalnu datoteku.
 *
 * @author Petar Kovač
 *
 */
public class Crypto {
	public static void main(String[] args) {
		if(args.length != 2 && args.length != 3) {
			printError();
			System.exit(0);
		}
		if(!args[0].equals("checksha") && !args[0].equals("encrypt") && !args[0].equals("decrypt")) {
			printError();
			System.exit(0);
		}
		
		Path src = Paths.get("./src/main/resources/" + args[1]);
		
		try(Scanner input = new Scanner(System.in)) {
			if(args[0].equals("checksha")) {
				System.out.printf("Please provide expected sha-256 digest for %s: %n> ", args[1]);
				
				String sha = input.next();
				String check = checkSha(src);
				
				System.out.println("Digesting completed. ");
				if(sha.equals(check)) {
					System.out.printf("Digest of " + args[1] + " matches expected digest.");
				} else {
					System.out.printf("Digest of " + args[1] + " does not match expected digest. Digest was: " + check);
					
				}
			}

			
			if(args[0].equals("encrypt") || args[0].equals("decrypt")) {
				
				//STAVITI U ŽELJENI DIREKTORIJ
				Path dest = Paths.get("C:/Users/Korisnik/UPJJ/" + args[2]);
				//STAVITI U ŽELJENI DIREKTORIJ
				
				Files.deleteIfExists(dest);
				Files.createFile(dest);
				
				System.out.printf("Please provide password as hex-encoded text (32 hex-digits): %n> ");
				String password = input.next();
				System.out.printf("Please provide initialization vector as hex-encoded text (32 hex-digits): %n> ");
				String initVector = input.next();
				
				if(args[0].equals("encrypt")) {
					encryptDecrypt(password, initVector, src, dest, true);
					System.out.printf("Encryption completed. Generated file %s based on file %s",args[2], args[1]);	
				} else {
					encryptDecrypt(password, initVector, src, dest, false);
					System.out.printf("Decryption completed. Generated file %s based on file %s",args[2], args[1]);
				}
			}
			
			
		} catch(IOException exc) {
			System.err.println("File cannot be read!");
			System.exit(0);
		}
	}
	
	/**
	 * Standardna poruka za grešku u slučaju pogrešnog unosa.
	 */
	private static void printError() {
		System.err.println("Atleast two arguments must be provided:");
		System.err.println("\"checksha\" + \"src filename\" for SHA-256 encryption.");
		System.err.println("\"encrypt\" + \"src filename\" + \"dest filename\" for AES encryption.");
		System.err.println("\"decrypt\" + \"src filename\" + \"dest filename\" for AES decryption.");
	}
	
	/**
	 * Računa digest jedne datoteke kako je definirano SHA-256 algoritmom.
	 * 
	 * @param src Izvorna datoteka.
	 * @return Digest generiran SHA-256 algoritmom.
	 * @throws IOException Ako se dogodi greška tijekom čitanja datoteke ili datoteka ne postoji.
	 */
	private static String checkSha(Path src) throws IOException {
		try (BufferedInputStream input = new BufferedInputStream(Files.newInputStream(src))) {
			
			MessageDigest sha = MessageDigest.getInstance("SHA-256");
			byte[] inputBytes = new byte[4096];
			int writtenBytes;
			while((writtenBytes = input.read(inputBytes)) != -1) {
				sha.update(inputBytes, 0, writtenBytes);
			}
			return Util.bytetohex(sha.digest());
			
		} catch (NoSuchAlgorithmException e) {
			System.out.println("No such algorithm!");
		}
		return null;
	}
	
	/**
	 * Metoda koja kriptira/dekriptira podatke iz izvorne datoteke u odredišnu.
	 * 
	 * @param password Ključ enkripcije.
	 * @param initVector Vektor enkripcije.
	 * @param src Izvorna datoteka.
	 * @param dest Odredišna datoteka.
	 * @param encrypt Ako je <code>true</code>, pokrenut će se proces enkripcije, 
	 * ako je <code>false</code>, izvorna datoteka će se dekriptirati.
	 * @throws IOException Ako se dogodi greška tijekom čitanja datoteke ili neka datoteka ne postoji.
	 */
	private static void encryptDecrypt(String password, String initVector, Path src, Path dest, boolean encrypt) throws IOException {
		try (BufferedInputStream input = new BufferedInputStream(Files.newInputStream(src)); 
			BufferedOutputStream output = new BufferedOutputStream(Files.newOutputStream(dest))) {
			
			Cipher cipher = initCipher(encrypt, password, initVector);
			if(cipher == null) return;
			
			try {
				codeDecode(cipher, input, output);
			} catch (IllegalBlockSizeException | BadPaddingException e) {
				System.out.println("Can't encrypt!");
			}
			
			
		}
	}
	

	/**
	 * Metoda koja inicijalizira Cipher koji se koristi za enkripciju.
	 * 
	 * @param encrypt Ako je <code>true</code>, cipher će vršiti enkripciju,
	 *  ako je <code>false</code>, cipher vrši dekripciju.
	 * @param password Ključ enkripcije.
	 * @param initVector Vektor enkripcije.
	 * @return Cipher koji se koristi za enkripciju.
	 */
	private static Cipher initCipher(boolean encrypt, String password,String initVector) {
		try {
			String keyText = password;
			String ivText = initVector;
			SecretKeySpec keySpec = new SecretKeySpec(Util.hextobyte(keyText), "AES");
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hextobyte(ivText));
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
			return cipher;
		} catch (Exception e) {
			System.out.println("Can't create cypher.");
		}
		return null;
	}
	
	/**
	 * Metoda u kojoj se događa enkripcija i ispisuje se u odredišnu datoteku.
	 * 
	 * @param cipher Cipher koji se koristi za enkripciju.
	 * @param input Tok iz izvorne datoteke.
	 * @param output Tok prema odredišnoj datoteci.
	 * @throws IOException Ako se dogodi greška tijekom čitanja datoteke.
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	private static void codeDecode(Cipher cipher, BufferedInputStream input, BufferedOutputStream output) throws IOException, IllegalBlockSizeException, BadPaddingException {
		byte[] inputBytes = new byte[4096];
		int writtenBytes;
		while((writtenBytes = input.read(inputBytes)) != -1) {
			output.write(cipher.update(inputBytes, 0, writtenBytes));
		}
		
		output.write(cipher.doFinal());
	}
}
