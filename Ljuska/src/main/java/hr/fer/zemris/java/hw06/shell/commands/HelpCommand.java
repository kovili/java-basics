package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.SimpleLexer;
/**
 * Naredba koja korisniku daje opise ostalih naredbi u MyShell-u.
 *
 * @author Petar Kovač
 *
 */
public class HelpCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []args = SimpleLexer.simpleTokenization(arguments);
		if(args.length > 1) {
			env.writeln("Help command must have one or no argument!");
			return ShellStatus.CONTINUE;
		}
		
		if(!args[0].isEmpty()) {
			if(env.commands().containsKey(args[0])) {
				env.write(String.format("The description of command '%s' is:%n%s%n", 
						env.commands().get(args[0]).getCommandName(), "=".repeat(40)));
				env.commands().get(args[0]).getCommandDescription().forEach((str) -> env.writeln(str));
				env.write(String.format("%s%n", "=".repeat(40)));
			} else {
				env.writeln("This command doesn't exits in MyShell.");
				return ShellStatus.CONTINUE;
			}
		} else {
			env.write(String.format("The supported arguments are:%n%s%n", "=".repeat(40)));
			env.commands().forEach((s, k) -> env.writeln(s));
			env.write(String.format("%s%n", "=".repeat(40)));
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "help";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("This command provides general information about other commands.");
		desc.add("It can have one or no arguments.");
		desc.add("If initiated with no arguments, the user will be provided with");
		desc.add("a list of all the commands this shell supports.");
		desc.add("If initiated with one command which is a command name");
		desc.add("it will give user a description of the command.");
		return Collections.unmodifiableList(desc);
	}

}
