package hr.fer.zemris.java.hw06.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Jednostavan Parser koji koristi {@link SimpleLexer} za tokenizaciju.
 *
 * @author Petar Kovač
 */
public class PathParser {
	//Token čijem si primitkom zaustavlja parsiranje.
	private static final String ENDTOKEN = null;
	/**
	 * Lekser kojeg ovaj parser koristi.
	 */
	SimpleLexer lexer;
	/**
	 * Tokenizirana naredba.
	 */
	String []tokens;

	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param arg Argument jedne naredbe.
	 * @throws NullPointerException ako je ulazni argument jednak <code>null</code>.
	 */
	public PathParser(String arg) {
		this.lexer = new SimpleLexer(arg);
		tokens = tokenize(arg);
	}
	
	/**
	 * Metoda koja prima tokene od {@link SimpleLexer}-a dok ne dođe do tokena ENDTOKEN.
	 * @param arg String koji se parsira, u ovoj metodi se samo vrši jednostavnja provjera nad argumentom.
	 * Ako argument nije oblika | "argument1" argument2  |, tj. ako nema 2 navodna znaka s nečime između,
	 * nad njime će se izvršiti jednostavna tokenizacija.
	 * @return Polje tokeniziranih String-ova.
	 */
	private String[] tokenize(String arg) {
		if(!(arg.matches(".*\".*\".*"))) {
			return SimpleLexer.simpleTokenization(arg);
		}
		List<String> allTokens = new ArrayList<>();
		try {
			String token;
			while(!((token = lexer.nextToken()) == ENDTOKEN)) {
				allTokens.add(token);
			}
			String [] tokenized = new String[allTokens.size()];
			return allTokens.toArray(tokenized);
		} catch(LexerException e) {
			throw new ParserException(e.getMessage());
		}
	}
	
	public String[] getTokens() {
		return tokens;
	}
}
