package hr.fer.zemris.java.hw06.util;
/**
 * Iznimka koja se baca kada se dogodi pogreška u tokeniziranju.
 * 
 * @author Petar Kovač
 */
public class LexerException extends RuntimeException{

	
	private static final long serialVersionUID = 1L;

	public LexerException() {
	}

	public LexerException(String message) {
		super(message);
	}
	
	public LexerException(Throwable cause) {
		super(cause);
	}
	
	public LexerException(String message, Throwable cause) {
		super(message, cause);
	}
}
