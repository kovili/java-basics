package hr.fer.zemris.java.hw06.shell.commands;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.ParserException;
import hr.fer.zemris.java.hw06.util.PathParser;
import hr.fer.zemris.java.hw06.util.Util;
/**
 * Naredba koja izbacuje heksadecimalni prikaz sadržaja neke datoteke.
 *
 * @author Petar Kovač
 *
 */
public class HexDumpCommand implements ShellCommand{
	private static final int HEX_DUMP_WIDTH = 76;
	private static final int MIN_ASCII_VAL = 32;
	private static final int MAX_ASCII_VAL = 127;
	private static final int LINE_DUMP_WIDTH = 16;
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []arg;
		try {
			arg = new PathParser(arguments).getTokens();
		} catch(ParserException e) {
			env.writeln(e.getMessage());
			return ShellStatus.CONTINUE;
		}
		if(arg.length != 1 || arg[0].isEmpty()) {
			env.writeln("Hexdump command must have one argument!");
			return ShellStatus.CONTINUE;
		}
		Path file = Paths.get(arg[0]);
		if(!(Files.isRegularFile(file))) {
			env.writeln("Unable to read from file.");
			return ShellStatus.CONTINUE;
		}
		
		try (BufferedInputStream input = new BufferedInputStream(Files.newInputStream(file))) {
			int readBytes, lineNumber = 0;
			byte []bytes = new byte[LINE_DUMP_WIDTH];
			env.write(String.format("Initiating hex dump.%n%s%n", "=".repeat(HEX_DUMP_WIDTH)));
			
			while((readBytes = input.read(bytes)) != -1) {
				printHexDump(bytes, lineNumber++, env, readBytes);
			}
			
		} catch (IOException e) {
			env.writeln("Error while reading file.");
			return ShellStatus.CONTINUE;
		}
		env.write(String.format("%s%nSuccessful hex dump%n", "=".repeat(HEX_DUMP_WIDTH)));
		return ShellStatus.CONTINUE;
	}


	@Override
	public String getCommandName() {
		return "hexdump";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("The hexdump command expects a single mandatory argument - file name and");
		desc.add("produces hex-output.");
		return Collections.unmodifiableList(desc);
	}
	
	/**
	 * Metoda koja printa hexdump po traženom formatu.
	 * 
	 * @param bytes Bajtovi iz datoteke.
	 * @param lineNumber Broj linije.
	 * @param env Sučelje prema korisniku.
	 * @param readBytes Broj pročitanih bajtova.
	 */
	private static void printHexDump(byte[] bytes, int lineNumber, Environment env, int readBytes) {
		env.write(String.format("%08x:", lineNumber * LINE_DUMP_WIDTH));
		StringBuilder builder = new StringBuilder();
		StringBuilder text = new StringBuilder();
		
		for(int i = 0; i < LINE_DUMP_WIDTH/2; i++) {
			builder.append(" ").append((readBytes <= i) ? "  " : Util.bytetohex(bytes[i]));
			
			if(readBytes > i) {
				text.append((bytes[i] < MIN_ASCII_VAL || bytes[i] > MAX_ASCII_VAL) 
							? "." : Character.valueOf((char) bytes[i]));
			} else {
				text.append(" ");
			}

		}
		
		builder.append("|");
		
		for(int i = LINE_DUMP_WIDTH/2; i < LINE_DUMP_WIDTH; i++) {
			builder.append((readBytes <= i) ? "  " : Util.bytetohex(bytes[i])).append(" ");
			if(readBytes > i) {
				text.append((bytes[i] < MIN_ASCII_VAL || bytes[i] > MAX_ASCII_VAL) 
							? "." : Character.valueOf((char) bytes[i]));
			} else {
				text.append(" ");
			}
		}
		
		builder.append("| ");
		builder.append(text.toString());
		env.writeln(builder.toString());
	}

}
