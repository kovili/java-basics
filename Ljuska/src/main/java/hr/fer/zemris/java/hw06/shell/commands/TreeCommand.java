package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.ParserException;
import hr.fer.zemris.java.hw06.util.PathParser;

/**
 * Naredba koja rekurzivno trči po stablu datoteka i ispisuje njihova imena.
 *
 * @author Petar Kovač
 *
 */
public class TreeCommand implements ShellCommand{
	private TreeVisitor visitor;

	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []arg;
		try {
			arg = new PathParser(arguments).getTokens();
		} catch(ParserException e) {
			env.writeln(e.getMessage());
			return ShellStatus.CONTINUE;
		}
		
		
		if(arg.length != 1 || arg[0].isEmpty()) {
			env.writeln("Tree command must have one argument!");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Path path = Paths.get(arg[0]);
			visitor = new TreeVisitor(env);
			Files.walkFileTree(path, visitor);
		} catch (IOException e) {
			env.writeln("Couldn't find directory.");
			return ShellStatus.CONTINUE;
		} catch (Exception e) {
			env.writeln("Error while listing directory");
			return ShellStatus.CONTINUE;
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "tree";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("The tree command prints a tree of given directory.");
		desc.add("It accepts only one mandatory command - path to directory");
		return Collections.unmodifiableList(desc);
	}
	
	/**
	 * Jednostavan FileVisitor koji 'šeće' po stablu direktorija i ispisuje imena svih datoteka.
	 *
	 * @author Petar Kovač
	 *
	 */
	private static class TreeVisitor extends SimpleFileVisitor<Path> {
		private int level;
		Environment env;
		
		/**
		 * Zadani konstruktor.
		 * @param env Sučelje prema korisniku.
		 */
		public TreeVisitor(Environment env) {
			this.env = env;
		}
		
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			env.write(String.format("|%s%s%n", " ".repeat(level * 2), dir.getFileName()));
			level++;
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			String spacing = " ".repeat(level * 2);
			env.write(String.format("|%s%s%n", spacing, file.getFileName()));
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			level--;
			return FileVisitResult.CONTINUE;
		}
	}
}
