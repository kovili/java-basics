package hr.fer.zemris.java.hw06.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.ParserException;
import hr.fer.zemris.java.hw06.util.PathParser;
/**
 * Naredba koja kopira sadržaj jedne datoteke na neko drugo mjesto-
 *
 * @author Petar Kovač
 *
 */
public class CopyCommand implements ShellCommand{

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []arg;
		try {
			arg = new PathParser(arguments).getTokens();
		} catch(ParserException e) {
			env.writeln(e.getMessage());
			return ShellStatus.CONTINUE;
		}
		
		if(arg.length != 2) {
			env.writeln("Copy command must have 2 arguments!");
			return ShellStatus.CONTINUE;
		}
		
		Path src = Paths.get(arg[0]);
		Path dest = Paths.get(arg[1]);
		try (BufferedInputStream input = new BufferedInputStream(Files.newInputStream(src))) {
			if(Files.isDirectory(src)) {
				env.writeln("First argument must be a file!");
				return ShellStatus.CONTINUE;
			}
			if(Files.isDirectory(dest)) {
				dest = Paths.get(arg[1] + "/" + src.getFileName());
			}
			String prompt = "Y";
			if(Files.exists(dest)) {
				env.writeln("File exists. Do you want to overwrite? Y/N");
				prompt = env.readLine();
			} 
			if(prompt.toUpperCase().equals("Y")) {
				BufferedOutputStream output = new BufferedOutputStream(Files.newOutputStream(dest));
				byte [] bytes = new byte[4096];
				int readBytes;
				while((readBytes = input.read(bytes)) != -1) {
					output.write(bytes, 0, readBytes);
				}
				output.close();
				env.writeln("File successfully copied.");
			}
		} catch (IOException e) {
			env.writeln("Couldn't find directory.");
			return ShellStatus.CONTINUE;
		}
				
		
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "copy";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("The copy command copies the given file to a destination, which can be a directory or file.");
		desc.add("It expect two arguments. The first argument must be the source file path and name.");
		desc.add("The second argument can be a file, in which case the user will be asked if the file can be overwritten,");
		desc.add("if the destination file exists. If the file doesn't exist, a copy of the source file will be created");
		desc.add("If the second argument is a directory, it will be assumed that the user wants to");
		desc.add("copy the file to given directory. The name of the copied file will be equal to the source file.");
		desc.add("In this case the rules given above still apply.");
		return Collections.unmodifiableList(desc);
	}

}
