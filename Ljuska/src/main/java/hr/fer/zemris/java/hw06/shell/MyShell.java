package hr.fer.zemris.java.hw06.shell;

import java.util.Scanner;

import hr.fer.zemris.java.hw06.shell.commands.ShellCommand;
import hr.fer.zemris.java.hw06.shell.environment.ShellEnvironment;
/**
 * Jednostavna implementacija programa s komandnom linijom i nekoliko jednsotavnih naredbi.
 * Implementirane naredbe su: symbol, charsets, cat, ls, tree, copy, mkdir, hexdump, help.
 *
 * @author Petar Kovač
 * @version v 3.1.0
 *
 */
public class MyShell {
	public static void main(String[] args) {
		try(Scanner input = new Scanner(System.in)) {
			
			//Inicijalizacija okruženja
			ShellEnvironment environment = new ShellEnvironment(input);
			environment.writeln("Welcome to MyShell v 1.0");
			ShellStatus status = ShellStatus.CONTINUE;
			do {
				try {
					environment.write(String.format("%c ", environment.getPromptSymbol()));
					String []line = environment.readLine().split("\\s+", 2);
					String arguments = "";
					
					if(line[0].contentEquals("exit")) {
						environment.writeln("Goodbye");
						System.exit(0);
					}
					
					//Prepoznavanje naredbi
					ShellCommand command = environment.commands().get(line[0]);
					if(command == null) {
						environment.writeln("No command recognized.");
						continue;
					}
					
					if(line.length == 2) {
						arguments = line[1];
					}
					
					//Konkatenacija višelinijskih naredbi
					if(arguments != "") {
						while(arguments.endsWith(Character.toString(environment.getMorelinesSymbol()))) {
							arguments = arguments.substring(0, arguments.length() - 1).concat(" ");
							environment.write(String.format("%c ", environment.getMultilineSymbol()));
							arguments = arguments.concat(environment.readLine());
						}
						
					}
					
					status = command.executeCommand(environment, arguments);
					//Ako dođe do greške tijekom komunikacije s korisnikom
				} catch(ShellIOException e) {
					System.out.println("Fatal error occurred. Terminating shell.");
					status = ShellStatus.TERMINATE;
				}
				
			} while(status != ShellStatus.TERMINATE);
		}
	} 
}
