package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.ParserException;
import hr.fer.zemris.java.hw06.util.PathParser;
/**
 * Naredba kojom se stvara datoteka na disku.
 *
 * @author Petar Kovač
 *
 */
public class MKDirCommand implements ShellCommand{

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []arg;
		try {
			arg = new PathParser(arguments).getTokens();
		} catch(ParserException e) {
			env.writeln(e.getMessage());
			return ShellStatus.CONTINUE;
		}
		
		if(arg.length != 1 || arg[0].isEmpty()) {
			env.writeln("Mkdir command must have one argument!");
			return ShellStatus.CONTINUE;
		}
		
		try {
			createDirTree(Paths.get(arg[0]));
		} catch (IOException  |  InvalidPathException e) {
			env.writeln("Directories could not be created.");
			return ShellStatus.CONTINUE;
		}
		env.writeln("Directory creation successful");
		return ShellStatus.CONTINUE;
		
		
		
	}

	@Override
	public String getCommandName() {
		return "mkdir";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("The command mkdir takes a single argument - directory name,");
		desc.add("and creates the appropriate directory structure");
		return Collections.unmodifiableList(desc);
	}
	
	private boolean createDirTree(Path path) throws IOException {
		if(path == null) {
			return false;
		}
		if(Files.exists(path)) {
			return true;
		}
		if(createDirTree(path.getParent())) {
			Files.createDirectory(path);
			return true;
		}
		return false;
		
	}
}
