package hr.fer.zemris.java.hw06.shell.commands;

import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.ParserException;
import hr.fer.zemris.java.hw06.util.PathParser;

/**
 * Naredba koja čita iz datoteke čija je staza dana kao argument i ispisuje sadržaj korisniku.
 *
 * @author Petar Kovač
 *
 */
public class CatCommand implements ShellCommand{

	/**
	 * Čita iz datoteke dok ne dođe do kraja datoteke.
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []arg;
		try {
			arg = new PathParser(arguments).getTokens();
		} catch(ParserException e) {
			env.writeln(e.getMessage());
			return ShellStatus.CONTINUE;
		}
		
		
		if(arg.length != 1 && arg.length != 2 || arg[0].isEmpty()) {
			env.writeln("Cat command must have one or two arguments!");
			return ShellStatus.CONTINUE;
		}
		
		Charset cset = Charset.defaultCharset();
		if(arg.length == 2) {
			Charset newCSet = Charset.availableCharsets().get(arg[1]);
			cset = newCSet == null ? cset : newCSet;
		}
		
		
		try(BufferedReader input = Files.newBufferedReader(Paths.get(arg[0]), cset)){
			int i = 1;
			env.writeln(String.format("READING CONTENTS FROM FILE %s:%n%s%n", Paths.get(arg[0]).getFileName(), "=".repeat(30)));
			while(input.ready()) {
				env.writeln(String.format("LINE %d: %S", i++, input.readLine()));
			}
		} catch (Exception e) {
			env.writeln("File could not be opened.");
			return ShellStatus.CONTINUE;
		}
		env.writeln(String.format("%n%s%nCONTENTS FROM FILE READ.%n", "=".repeat(30)));
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "cat";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("This command opens given file and writes the content to console.");
		desc.add("It takes one or two arguments, the first argument is mandatory.");
		desc.add("The first argument is a path to some file. The second argument is a charset");
		desc.add("name that should be used to interpret chars from bytes.");
		desc.add("If the second argument is not provided, a default platform charset will be used");
		return Collections.unmodifiableList(desc);
	}

}
