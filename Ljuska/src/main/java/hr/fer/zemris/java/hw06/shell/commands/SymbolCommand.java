package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.SimpleLexer;
/**
 * Naredba preko koje je moguće promijeniti jedan od simbola MyShell okruženja.
 *
 * @author Petar Kovač
 *
 */
public class SymbolCommand implements ShellCommand{

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []args = SimpleLexer.simpleTokenization(arguments);
		if(args.length != 1 && args.length != 2 || args[0].isEmpty()) {
			env.writeln("Symbol command must have one or two arguments!");
			return ShellStatus.CONTINUE;
		}
		
		Character oldSign;
		try {
			args = checkArgument(args);
			oldSign = getOldSign(args[0], env, args[1]);
		} catch(IllegalArgumentException e) {
			env.writeln(e.getMessage());
			return ShellStatus.CONTINUE;
		}
		
		if(args[1] == null) {
			env.writeln(String.format("Symbol for %s is '%c'", args[0], oldSign));
		} else {
			env.writeln(String.format("Symbol for %s changed from '%c' to '%c'", args[0], oldSign, args[1].charAt(0)));
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "symbol";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<String>();
		desc.add("Command changes one of the PROMPTSYMBOL/MULTILINESYMBOL/MORELINESYMBOL symbols to some other symbol");
		desc.add("If only one argument equal to PROMPTSYMBOL/MULTILINESYMBOL/MORELINESYMBOL is given after symbol keyword,");
		desc.add("The current symbol for chosen type will be printed.");
		desc.add("If two arguments are given, first equal to PROMPTSYMBOL/MULTILINESYMBOL/MORELINESYMBOL,");
		desc.add("and the second equal to some Character,");
		desc.add("the chosen type symbol will be changed to new simbol defined by user with the second argument.");
		return Collections.unmodifiableList(desc);
	}
	
	private static String [] checkArgument(String []arg) {
		String firstArg = null;
		String secondArg = null;
		if(arg[0].equals("PROMPT")) {
			firstArg = "PROMPT";
		} else if(arg[0].equals("MULTILINE")) {
			firstArg = "MULTILINE";
		} else if(arg[0].equals("MORELINES")) {
			firstArg = "MORELINES";
		} else {
			throw new IllegalArgumentException("Can't recognize symbol type!");
		}
		
		if(arg.length == 2) {
			if(arg[1].matches(".{1}")) {
				secondArg = arg[1];
			} else {
				throw new IllegalArgumentException("Second argument must be Character!");
			}
		}
		String[] output = {firstArg, secondArg};
		return output;	
	}
	
	private static Character getOldSign(String arg, Environment env, String newSign) {
		Character oldSymbol;
		if(arg.equals("PROMPT")) {
			oldSymbol = env.getPromptSymbol();
			env.setPromptSymbol(newSign == null ? oldSymbol : newSign.charAt(0));
		} else if(arg.equals("MULTILINE")) {
			oldSymbol = env.getMultilineSymbol();
			env.setMultilineSymbol(newSign == null ? oldSymbol : newSign.charAt(0));
		} else if(arg.equals("MORELINES")) {
			oldSymbol = env.getMorelinesSymbol();
			env.setMorelinesSymbol(newSign == null ? oldSymbol : newSign.charAt(0));
		} else {
			throw new IllegalArgumentException("Can't recognize symbol type!");
		}
		return oldSymbol;
	}


}
