package hr.fer.zemris.java.hw06.util;

import java.util.Objects;

/**
 * Jednostavan lekser za grupiranje riječi i stringova.
 *
 * @author Petar Kovač
 *
 */
public class SimpleLexer {
	//Zadnji token koji se može legalno baciti.
	private static final String ENDTOKEN = null;
	/**
	 * Polje znakova ulaznog teksta.
	 */
	private char []data;
	/**
	 * Trenutni indeks u polju znakova.
	 */
	private int currentIndex;
	
	/**
	 * Zadani konstruktor.
	 * @param data Ulazni tekst.
	 * @throws NullPointerException ako je ulazni String jednak <code>null</code>.
	 */
	public SimpleLexer(String data) {
		Objects.requireNonNull(data);
		this.data = data.toCharArray();
	}
	
	/**
	 * Metoda koja vraća sljedeću riječ ili String.
	 * 
	 * @return Sljedeći token.
	 */
	public String nextToken() {
		if(data.length <= currentIndex) {
			return ENDTOKEN;
		}
		
		skipBlanks();
		
		if(data.length <= currentIndex) {
			return ENDTOKEN;
		}
		
		if(data[currentIndex] == '\"') {
			currentIndex++;
			return parseString();
		}
		
		return parseWord();
	}
	
	/**
	 * Metoda koja preskače praznine.
	 */
	private void skipBlanks() {
		if(!(Character.isWhitespace(data[currentIndex]) || currentIndex == 0)) {
			throw new LexerException("Command must have spaces between arguments.");
		}
		while(Character.isWhitespace(data[currentIndex])) {
			currentIndex++;
			if(currentIndex == data.length) {
				return;
			}
		}
	}
	
	/**
	 * Metoda koja se poziva kada se naiđe na String.
	 * Grupira sve što je unutar Stringa i rješava escapeanje.
	 * 
	 * @return Grupirani tekst.
	 */
	private String parseString() {
		StringBuilder builder = new StringBuilder();
		while(currentIndex != data.length) {
			if(!(data[currentIndex] == '\"')) {
				if(data[currentIndex] == '\\') {
					builder.append(resolveEscaping());
					currentIndex++;
				} else {
					builder.append(data[currentIndex]);
				}
				currentIndex++;
			} else {
				currentIndex++;
				return builder.toString();
			}
		}
		throw new LexerException("String must end with \"");
	}
	
	/**
	 * Metoda koja rješava escapanje po pravilima:
	 * "\" + "x"  = "\x", gdje je x bilo koji znak osim navodnog znaka ili escape znaka.
	 * "\" + "\"  = "\"
	 * "\" + "\" = '"'
	 * Ova pravila vrijede samo unutar Stringa.
	 * 
	 * @return Riješen primjerak escape-a.
	 */
	private String resolveEscaping() {
		if(currentIndex + 1 == data.length) {
			throw new LexerException("String must end with \"");
		}
		if(data[currentIndex + 1] == '\"') {
			return "\"";
		}
		if(data[currentIndex + 1] == '\\') {
			return "\\";
		}
		else return "\\".concat(Character.toString(data[currentIndex + 1]));
	}
	
	/**
	 * Metoda grupira jednu riječ tako da traži sljedeći razmak.
	 * 
	 * @return Riječ.
	 */
	private String parseWord() {
		StringBuilder builder = new StringBuilder();
		while(currentIndex != data.length) {
			if(!Character.isWhitespace(data[currentIndex])) {
				builder.append(data[currentIndex++]);
			} else {
				break;
			}
		}
		return builder.toString();
	}
	
	/**
	 * Metoda koja se koristi za najednostavniju tokenizaciju po razmacima.
	 * U pravilu se treba koristiti samo kada nema String-a u argumentu.
	 * 
	 * @param input Argument koji se treba parsirati.
	 * @return Parsirani argument.
	 */
	public static String[] simpleTokenization(String input) {
		return input.trim().split("\\s+");
	}
}
