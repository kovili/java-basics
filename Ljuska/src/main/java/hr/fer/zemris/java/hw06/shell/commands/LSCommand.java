package hr.fer.zemris.java.hw06.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;
import hr.fer.zemris.java.hw06.util.ParserException;
import hr.fer.zemris.java.hw06.util.PathParser;
/**
 * Naredba koja prikazuje svako dijete prve razine neke datoteke.
 *
 * @author Petar Kovač
 *
 */
public class LSCommand implements ShellCommand{

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String []arg;
		try {
			arg = new PathParser(arguments).getTokens();
		} catch(ParserException e) {
			env.writeln(e.getMessage());
			return ShellStatus.CONTINUE;
		}
		if(arg.length != 1 || arg[0].isEmpty()) {
			env.writeln("LS command must have one argument!");
			return ShellStatus.CONTINUE;
		}
		
		try {
			DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(arg[0]));
			env.write(String.format("LISTING FILES IN DIRECTORY %s:%n%s%n", 
									Paths.get(arg[0]).getFileName(), "=".repeat(40)));
			for(Path file : stream) {
				env.write(determineFileType(file.toFile()));
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				BasicFileAttributeView faView = Files.getFileAttributeView(
				file, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
				BasicFileAttributes attributes = faView.readAttributes();
				FileTime fileTime = attributes.creationTime();
				String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
				
				env.write(String.format(" %10d", attributes.size()));
				env.write(String.format(" %s", formattedDateTime));
				env.write(String.format(" %s%n", file.getFileName()));
				
			}
			env.write(String.format("%s%nLISTING COMPLETED.%n", "=".repeat(40)));
		} catch (IOException e) {
			env.writeln("Directory not found.");
			return ShellStatus.CONTINUE;
		} catch (SecurityException e) {
			env.writeln("You are not allowed to access this directory.");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "ls";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("The ls command creates a file listing of the current directory.");
		desc.add("The command takes only one argument - a path to a directory.");
		desc.add("The file listing also contains information about files or directories in the directory.");
		return Collections.unmodifiableList(desc);
	}
	
	private String determineFileType(File file) {
		String type = "";
		type = type.concat(file.isDirectory() ? "d" : "-");
		type = type.concat(file.canRead() ? "r" : "-");
		type = type.concat(file.canWrite() ? "w" : "-");
		type = type.concat(file.canExecute() ? "x" : "-");
		return type;
	}

}
