package hr.fer.zemris.java.hw06.shell.environment;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.ShellIOException;
import hr.fer.zemris.java.hw06.shell.commands.CatCommand;
import hr.fer.zemris.java.hw06.shell.commands.CharsetCommand;
import hr.fer.zemris.java.hw06.shell.commands.CopyCommand;
import hr.fer.zemris.java.hw06.shell.commands.HelpCommand;
import hr.fer.zemris.java.hw06.shell.commands.HexDumpCommand;
import hr.fer.zemris.java.hw06.shell.commands.LSCommand;
import hr.fer.zemris.java.hw06.shell.commands.MKDirCommand;
import hr.fer.zemris.java.hw06.shell.commands.ShellCommand;
import hr.fer.zemris.java.hw06.shell.commands.SymbolCommand;
import hr.fer.zemris.java.hw06.shell.commands.TreeCommand;

/**
 * Okruženje preko kojeg se vrši komunikacija između korisnika i programa MyShell.
 *
 * @author Petar Kovač
 */
public class ShellEnvironment implements Environment{
	private Scanner input;
	private Character MultilineSymbol = '|';
	private Character PromptSymbol = '>';
	private Character MorelinesSymbol = '\\';
	SortedMap<String, ShellCommand> commands;
	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param input Scanner preko kojeg se vrši komunikacija s korisnikom.
	 * @param commands Mapa svih mogućih naredbi.
	 */
	public ShellEnvironment(Scanner input) {
		this.input = input;
		this.commands = new TreeMap<>();
		commands.put("symbol", new SymbolCommand());
		commands.put("charsets", new CharsetCommand());
		commands.put("cat", new CatCommand());
		commands.put("ls", new LSCommand());
		commands.put("tree", new TreeCommand());
		commands.put("copy", new CopyCommand());
		commands.put("mkdir", new MKDirCommand());
		commands.put("hexdump", new HexDumpCommand());
		commands.put("help", new HelpCommand());
	}
	


	@Override
	public String readLine() throws ShellIOException {
		try {
			return input.nextLine();
		} catch (NoSuchElementException e) {
			throw new ShellIOException();
		}
	}

	@Override
	public void write(String text) throws ShellIOException {
		System.out.printf(text);
	}

	@Override
	public void writeln(String text) throws ShellIOException {
		System.out.println(text);
		
	}

	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	@Override
	public Character getMultilineSymbol() {
		return MultilineSymbol;
	}

	@Override
	public void setMultilineSymbol(Character symbol) {
		this.MultilineSymbol = symbol;
	}

	@Override
	public Character getPromptSymbol() {
		return PromptSymbol;
	}

	@Override
	public void setPromptSymbol(Character symbol) {
		this.PromptSymbol = symbol;
	}

	@Override
	public Character getMorelinesSymbol() {
		return MorelinesSymbol;
	}

	@Override
	public void setMorelinesSymbol(Character symbol) {
		this.MorelinesSymbol = symbol;
	}
	
}
