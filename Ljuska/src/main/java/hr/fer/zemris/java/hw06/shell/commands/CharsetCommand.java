package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;

import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.environment.Environment;

/**
 * Naredba čiji je posao ispisati sve Charsetove koji postoje na ovoj Java platformi.
 *
 * @author Petar Kovač
 *
 */
public class CharsetCommand implements ShellCommand{
	
	/**
	 * Vraća sve Charsetove koji posotje na ovoj Java platformi abecednim redoslijedom.
	 * 
	 * @param env {@link Environment} okruženje u kojem se izvodi ova naredba.
	 * @param arguments Argumenti naredbe -> Ova naredba ne koristi argumente.
	 * 
	 * @return ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(!(arguments.isEmpty())) {
			env.writeln("Ignoring arguments.");
		}
		SortedMap<String, Charset> charsets = Charset.availableCharsets();
		env.writeln("=".repeat(30));
		charsets.forEach((s, k) -> env.writeln(String.format("####|%-20s|####", s)));
		env.writeln("=".repeat(30));
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "charsets";
	}
	
	@Override
	public List<String> getCommandDescription() {
		List<String> desc = new ArrayList<>();
		desc.add("Command 'charsets' lists names of supported charsets for your Java platform.");
		desc.add("A single charset name is written per line.");
		desc.add("The charsets are written in alphabetical order");
		return Collections.unmodifiableList(desc);
	}

}
