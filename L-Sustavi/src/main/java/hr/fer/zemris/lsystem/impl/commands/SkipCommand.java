package hr.fer.zemris.lsystem.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.math.Vector2D;
/**
 * Naredba. Miče kornjaču za dani step.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class SkipCommand implements Command{
	/**
	 * Step koji definira pomak kornjače.
	 */
	double step;
	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param step {@link #step}
	 */
	public SkipCommand(double step) {
		this.step = step;
	}
	
	/**
	 * Miče kornjaču i pamti novu poziciju u trenutnom stanju kornjače.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Vector2D position = ctx.getCurrentState().getPosition();
		Vector2D offset = ctx.getCurrentState().getAngle().scaled(step);
		position.translate(offset);
	}
	
}