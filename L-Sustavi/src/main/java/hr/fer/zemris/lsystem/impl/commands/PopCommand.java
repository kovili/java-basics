package hr.fer.zemris.lsystem.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Naredba. Biše jedno kornačinjo stanje sa stoga.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class PopCommand implements Command{

	/**
	 * Briše jedno stanje sa stoga.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.popState();
	}

}
