package hr.fer.zemris.lsystem.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Naredba. Skalira kornjačin korak za dani faktor.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class ScaleCommand implements Command{
	/**
	 * Faktor kojim se skalira kornjačin pomak.
	 */
	double factor;
	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param factor {@link #factor}
	 */
	public ScaleCommand(double factor) {
		this.factor = factor;
	}

	/**
	 * Skalira kornjačin korak.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		double effectiveLength = ctx.getCurrentState().getEffectiveLength();
		ctx.getCurrentState().setEffectiveLength(effectiveLength * factor);
	}

}
