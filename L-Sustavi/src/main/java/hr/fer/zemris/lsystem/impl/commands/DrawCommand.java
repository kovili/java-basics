package hr.fer.zemris.lsystem.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;
/**
 * Naredba. Crta crtu zadane boje za dani step.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class DrawCommand implements Command{
	/**
	 * Step koji definira pomak kornjače.
	 */
	double step;
	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param step {@link #step}
	 */
	public DrawCommand(double step) {
		this.step = step;
	}
	
	/**
	 * Crta crtu zadane boje i pamti novu poziciju u trenutnom stanju kornjače.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState currentState = ctx.getCurrentState();
		Vector2D oldPosition = currentState.getPosition().copy();
		Vector2D offset = currentState.getAngle().scaled(step * currentState.getEffectiveLength());
		Vector2D newPosition = currentState.getPosition();
		newPosition.translate(offset);
		painter.drawLine(oldPosition.getX(), oldPosition.getY(), 
						newPosition.getX(), newPosition.getY(), 
						currentState.getColor(), 1);
	}
	
}
