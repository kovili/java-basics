package hr.fer.zemris.lsystem.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Naredba. Stavlja jedno kornjačino stanje na stog.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class PushCommand implements Command{

	/**
	 * Stavlja jedno stanje na stog.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.pushState(ctx.getCurrentState().copy());
	}

}