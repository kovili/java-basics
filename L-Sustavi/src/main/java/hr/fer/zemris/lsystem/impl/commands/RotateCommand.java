package hr.fer.zemris.lsystem.impl.commands;

import hr.fer.zemris.lsystems.Painter; 
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Naredba. Rotira kornjaču za dani kut u radijanima.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class RotateCommand implements Command{
	/**
	 * Kut u radijanima.
	 */
	private double angle;
	
	/**
	 * Zadani konstruktor.
	 * @param angle {@link #angle}
	 */
	public RotateCommand(double angle) {
		this.angle = angle;
	}
	/**
	 * Rotira kornjaču za dani kut u radijanima.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().getAngle().rotate(angle);
	}

}