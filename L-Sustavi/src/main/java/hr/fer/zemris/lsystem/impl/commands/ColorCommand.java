package hr.fer.zemris.lsystem.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Naredba. Mijenja boju kojom kornjača crta.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */
public class ColorCommand implements Command{ 
	/*
	 * Nova bojom kojom kornjača crta. 
	 */
	private Color color;

	/**
	 * Zadani konstruktor.
	 * 
	 * @param color {@link #color}
	 */
	public ColorCommand(Color color) {
		this.color = color;
	}

	/**
	 * Mijenja boju kojom kornjača crta.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setColor(color);
	}

}
