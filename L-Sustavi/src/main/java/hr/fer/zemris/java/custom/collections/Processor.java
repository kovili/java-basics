package hr.fer.zemris.java.custom.collections;
/**
 * Parametrizirano sučelje - definira objekt koji je sposoban obaviti neku operaciju nad danim objektom.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
@FunctionalInterface
public interface Processor<T> {
	/**
	 * Radi nedefinirani posao nad objektom.
	 * 
	 * @param value Objekt nad kojim se vrši neka operacija
	 */
	<E extends T> void process(E value);

}
