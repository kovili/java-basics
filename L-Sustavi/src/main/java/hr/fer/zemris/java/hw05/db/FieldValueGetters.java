package hr.fer.zemris.java.hw05.db;

/**
 * Konkretne strategije za {@link IFieldValueGetter}.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class FieldValueGetters {
	/**
	 * Strategija koja dohvaća ime studenta iz {@link StudentRecord}.
	 */
	public static final IFieldValueGetter FIRST_NAME = (student) -> student.getFirstName();
	
	/**
	 * Strategija koja dohvaća prezime studenta iz {@link StudentRecord}.
	 */
	public static final IFieldValueGetter LAST_NAME = (student) -> student.getLastName();
	
	/**
	 * Strategija koja dohvaća jmbag studenta iz {@link StudentRecord}.
	 */
	public static final IFieldValueGetter JMBAG = (student) -> student.getJmbag();
}
