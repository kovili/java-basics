package hr.fer.zemris.java.hw05.db;

/**
 * Klasa modelira jedan uvjetni izraz.
 * 
 * @author Petar Kovač
 * @version 1.0
 */

public class ConditionalExpression {
	/**
	 * {@link IFieldValueGetter}
	 */
	private IFieldValueGetter fieldGetter;
	/**
	 * Tekst nad kojim se radi upit.
	 */
	private String expression;
	/**
	 * {@link IComparisonOperator}
	 */
	private IComparisonOperator operator;
	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param getter {@link #getter}
	 * @param expression {@link #expression}
	 * @param operator {@link #operator}
	 */
	public ConditionalExpression(IFieldValueGetter fieldGetter, String expression, IComparisonOperator operator) {
		this.fieldGetter = fieldGetter;
		this.expression = expression;
		this.operator = operator;
	}

	/**
	 * @return {@link IFieldValueGetter}
	 */
	public IFieldValueGetter getFieldGetter() {
		return fieldGetter;
	}
	
	/**
	 * @return {@link #expression}
	 */
	public String getStringLiteral() {
		return expression;
	}
	
	/**
	 * @return {@link IComparisonOperator}
	 */
	public IComparisonOperator getComparisonOperator() {
		return operator;
	}
	
}
