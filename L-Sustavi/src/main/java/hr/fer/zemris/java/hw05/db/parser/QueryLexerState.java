package hr.fer.zemris.java.hw05.db.parser;
/**
 * Opisuje sva moguća stanja lexera za učitavanje query naredbi
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public enum QueryLexerState {
	/**
	 * Stanje kada se učitava običan tekst.
	 */
	WORD,
	/**
	 * Stanje nakon čitanja jednog String-a.
	 */
	AND,
	/**
	 * Stanje nakon čitanja atributa.
	 */
	OPERATOR,
	/**
	 * Stanje nakon čitanja operatora.
	 */
	LITERAL
}

