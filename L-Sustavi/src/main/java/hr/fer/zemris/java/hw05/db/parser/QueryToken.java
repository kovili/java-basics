package hr.fer.zemris.java.hw05.db.parser;

/**
 * Modelira jedan Token za query naredbe.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class QueryToken {
	/**
	 * {@link QueryTokenType} ovog tokena.
	 */
	private QueryTokenType type;
	/**
	 * Vrijednost zapisana u tokenu.
	 */
	private String value;
	
	/**
	 * Zadan konstruktor.
	 * 
	 * @param type {@link #type}
	 * @param value {@link #value}
	 */
	public QueryToken(QueryTokenType type, String value) {
		this.type = type;
		this.value = value;
	}
	/**
	 * @return {@link QueryTokenType}
	 */
	public QueryTokenType getType() {
		return type;
	}
	/**
	 * @return {@link #value}
	 */
	public String getValue() {
		return value;
	}
	
	
	
	
}
