package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList; 
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Modelira jednu bazu podataka.
 *
 * @author Petar Kovač
 * @version 1.0
 *
 */
public class StudentDatabase {
	/**
	 * Lista zapisa o studentima.
	 */
	private List<StudentRecord> studentRecords;
	/**
	 * Indeks za brz dohvat informacije o studentu po jmbag-u.
	 */
	private HashMap<String, StudentRecord> index;
	
	/**
	 * Zadani konstruktor. Prima sve linije zapisa o studentima.
	 * 
	 * @param records Zapisi o studentima.
	 * @throws NullPointerException Ako je argument jednak <code>null</code>.
	 */
	public StudentDatabase(String []records) {
		Objects.requireNonNull(records);
		studentRecords = new ArrayList<>();
		index = new HashMap<>();
		
		
		for(String record : records) {
			String []entry = record.split("\\s+");
			checkData(entry);
			
			int length = entry.length;
			int finalGrade = Integer.parseInt(entry[length - 1]);
			String jmbag = entry[0];
			String firstName = entry[length - 2];
			StudentRecord newRecord = new StudentRecord(firstName, extractLastName(entry), jmbag, finalGrade);
			
			studentRecords.add(newRecord);
			index.put(jmbag, newRecord);
		}
	}
	
	/**
	 * Provjerava je li unšenena linija podatka korektna.
	 * Ako nisu izlazi se iz programa.
	 * Trebali bi se unijeti samo podatci koji imaju jmbag, prezime, ime i ocjenu tim redom.
	 * Jmbagovi koji su već unešeni ne smiju ponovo biti unešeni.
	 * Završne ocjene mogu biti samo ∈{1, 2, 3, 4, 5}.
	 * 
	 * @param entry Riječi u liniji.
	 */
	private void checkData(String []entry) {
		if(entry.length < 4) {
			System.out.println("Line must have atleast 4 elements!");
			System.exit(0);
		}
		if(index.containsKey(entry[0])) {
			System.out.println("Duplicate jmbag entered.");
			System.exit(0);
		}
		int grade = Integer.parseInt(entry[entry.length - 1]);
		if(grade < 1 || grade > 5) {
			System.out.println("Out of range grade entered!");
			System.exit(0);
		}
	}
	
	/**
	 * Izvlači prezime iz jedne linije koja je korektno napisana.
	 * 
	 * @param entry Riječi u liniji.
	 * @return Prezime.
	 */
	private String extractLastName(String []entry) {
		String lastName = "";
		for(int i = 1, limit = entry.length - 2; i < limit; i++) {
			lastName = lastName.concat(entry[i]).concat(" ");
		}
		return lastName.strip();
	}
	
	/**
	 * Dohvaća {@link StudentRecord} koji je povezan s danim JMBAG-om.
	 * 
	 * @param jmbag 
	 * @return {@link StudentRecord} povezan s danim JMBAG-om.
	 */
	public StudentRecord forJMBAG(String jmbag) {
		StudentRecord record = index.get(jmbag);
		if(!(record == null)) {
			System.out.println("Using index for record retrieval.");
		}
		return record;
	}
	
	/**
	 * Filitrira {@link StudentRecord} zapise o studentima po nekom uvjetu.
	 * 
	 * @param filter Apstraktna strategija.
	 * @return Zapisi {@link StudentRecord} koji zadovoljavaju dani uvjet.
	 */
	public List<StudentRecord> filter(IFilter filter) {
		List<StudentRecord> acceptable = new ArrayList<>();
		for(StudentRecord record : studentRecords) {
			if(filter.accepts(record)) {
				acceptable.add(record);
			}
		}
		return acceptable;
	}
}
