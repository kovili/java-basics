package hr.fer.zemris.java.custom.collections;

/**
 * Predstavlja stog na koji se mogu stavljati objekti.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */
public class ObjectStack <T> {
	/**
	 * Parametrizirana kolekcija koja čuva elemente.
	 */
	private ArrayIndexedCollection<T> array;
	/**
	 * Pretpostavljeni konstruktor.
	 */
	public ObjectStack() {
		array = new ArrayIndexedCollection<T>();
	}
	
	/**
	 * Provjerava je li stog prazan.
	 * 
	 * @return <code> true </code> ako je stog prazan, inače <code> false </code>
	 */
	public boolean isEmpty() {
		return array.isEmpty();
	}
	/**
	 * Daje veličinu stoga.
	 * 
	 * @return size Veličina stoga
	 */
	public int size() {
		return array.size();
	}
	
	/**
	 * Stavlja jedan objekt na vrh stoga. Prosječno vrijeme izvođenja je O(1).
	 * 
	 * @param value Objekt kojeg se stavlja na stog
	 * @throws NullPointerException ako je kao argument dana null vrijednost
	 */
	public void push(T value) {
		if(value == null) {
			throw new NullPointerException("Stack cannot accept null value!");
		}
		array.add(value);
	}
	
	/**
	 * Uklanja jedan element s vrha stoga i vraća ga korisniku. Prosječno vrijeme izvođenja je O(1).
	 * 
	 * @return <T> Objekt s vrha stoga
	 * @throws EmptyStackException ako je stog prazan
	 */
	public T pop() {
		T object = peek();
		array.remove(array.size() - 1);
		return object;
	}
	
	/**
	 * Daje korisniku element s vrha stoga, ali ga ne uklanja. Prosječno vrijeme izvođenja je O(1).
	 * 
	 * @return <T> Objekt s vrha stoga
	 * @throws EmptyStackException ako je stog prazan
	 */
	public T peek() {
		if(array.size() == 0) {
			throw new EmptyStackException("The stack is empty!");
		}
		return array.get(array.size() - 1);
	}
	
	/**
	 * Uklanja sve objekte sa stoga.
	 */
	public void clear() {
		array.clear();
	}
	
	

}
