package hr.fer.zemris.java.custom.collections;

/**
 * Modelira objekt koji ispituje je li neki objekt prihvatljiv pod danim uvjetima
 * 
 * @author Petar Kovač
 * @version 1.0
 */
@FunctionalInterface
public interface Tester<E> {

	/**
	 * Metoda koja testira prihvatljivost objekta.
	 * @param obj Objekt kojeg se testira
	 * @return <code> true </code> ako je objekt prošao testo, inače <code> false </code>
	 */
	<T extends E> boolean test(T obj);
}
