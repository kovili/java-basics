package hr.fer.zemris.java.hw05.db;

import hr.fer.zemris.java.hw05.db.parser.ParserException;

/**
 * Konkretne strategije za {@link IComparisonOperator}.
 * Podržava operacije usporedbe '<', '<=', '>' '>=', '==', '!=', LIKE.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class ComparisonOperators {
	
	/**
	 * Implementacija strategije koja će provjeriti je li prvi String leksikografski manji od drugog.
	 */
	public static final IComparisonOperator LESS = (s1, s2) -> s1.compareTo(s2) < 0;
	
	/**
	 * Implementacija strategije koja će provjeriti je li prvi String leksikografski manji ili jednak drugom.
	 */
	public static final IComparisonOperator LESS_OR_EQUALS = (s1, s2) -> s1.compareTo(s2) <= 0;
	
	/**
	 * Implementacija strategije koja će provjeriti je li prvi String leksikografski veći od drugog.
	 */
	public static final IComparisonOperator GREATER = (s1, s2) -> s1.compareTo(s2) > 0;
	
	/**
	 * Implementacija strategije koja će provjeriti je li prvi String leksikografski veći ili jednak drugom.
	 */
	public static final IComparisonOperator GREATER_OR_EQUALS = (s1, s2) -> s1.compareTo(s2) >= 0;
	
	/**
	 * Implementacija strategije koja će provjeriti je li prvi String leksikografski jednak drugom.
	 */
	public static final IComparisonOperator EQUALS = (s1, s2) -> s1.compareTo(s2) == 0;
	
	/**
	 * Implementacija strategije koja će provjeriti je li prvi String leksikografski različit od drugog.
	 */
	public static final IComparisonOperator NOT_EQUALS = (s1, s2) -> s1.compareTo(s2) != 0;
	
	/**
	 * Implementacija strategije koja će provjeriti je li prvi String sličan drugom Stringu po danom izrazu.
	 * LIKE podržava i znak '*' (wildcard) u drugom izrazu, što označuje pojavu nula ili više bilo kojih znakova na tom mjestu.
	 * Wildcard se smije pojaviti samo jednom u drugom Stringu, a u prvom se tretira kao normalni znak '*'
	 */
	public static final IComparisonOperator LIKE = (s1, s2) -> {
																s2 = s2.replaceFirst("\\*", ".*");
																if(s2.matches(".*[^.]\\*.*")) {
																	throw new ParserException("Literal can't have multiple wildcards!");
																}
																return s1.matches(s2);
																};
	
}
