package hr.fer.zemris.java.hw05.db.parser;

import java.util.Objects;
/**
 * Lekser za parsiranje query naredbi. Može grupirati samo poslije ključne riječi "query".
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class QueryLexer {
	/**
	 * {@link QueryToken} zadnji Token kojeg je ovaj leksera vratio.
	 */
	private QueryToken token;
	/**
	 * {@link QueryLexerState} trenutni režim rada leksera.
	 */
	private QueryLexerState state;
	/**
	 * Trenutna pozicija na kojem se lekser nalazi u naredbi.
	 */
	private int currentIndex;
	/**
	 * Polje znakova query naredbe.
	 */
	private char []data;
	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param data Tekstualni prikaz naredbe.
	 * @throws NullPointerException ako je kao argument dan <code>null</code>.
	 */
	public QueryLexer(String data) {
		Objects.requireNonNull(data);
		this.data = data.toCharArray();
	}
	
	/**
	 * Naredba generira i potom vraća sljedeći Token koji lekser može naći.
	 * 
	 * @return {@link QueryToken} sljedeći Token.
	 * @throws LexerException ako ne može pronaći nijedan Token ili 
	 * 		   ako je naredba pozvana kad lekser dođe do kraja datoteke.
	 */
	public QueryToken nextToken() {
		if(token != null && token.getType() == QueryTokenType.EOF) {
			throw new LexerException("No new tokens can be generated!");
		}
		
		skipBlanks();
		
		if(currentIndex == data.length) {
			token = new QueryToken(QueryTokenType.EOF, null);
			return token;
		}
		

		
		if(state == QueryLexerState.OPERATOR) {
			token = new QueryToken(QueryTokenType.OPERATOR, extractOperator());
			return token;
		}
		
		if(isQuotationMark() && state == QueryLexerState.LITERAL) {
			token = new QueryToken(QueryTokenType.LITERAL, extractLiteral());
			return token;
		}
		
		if(Character.isLetter(data[currentIndex]) && state == QueryLexerState.WORD) {
			token = new QueryToken(QueryTokenType.ATTRIBUTE, extractWord());
			return token;
		}
		
		if(state == QueryLexerState.AND) {
			String check;
			check = extractWord();
			if(!check.equalsIgnoreCase(("AND"))) {
				throw new LexerException("Lexer expected AND");
			}
			token = new QueryToken(QueryTokenType.AND, check);
			return token;
		}
		
		throw new LexerException("No valid token recognized!"); 
		
		
		
	}
	
	/**
	 * Preskače sve praznine u polju znakova.
	 */
	private void skipBlanks() {
		for(int limit = data.length; currentIndex < limit; currentIndex++) {
			if(!isBlank()) {
				break;
			}
		}
	}
	
	/**
	 * Provjerava je li neki znak na indeksu {@link #currentIndex} praznina.
	 * 
	 * @return <code>true</code> ako je znak praznina, <code>false</code> inače.
	 */
	private boolean isBlank() {
		Character checkChar = data[currentIndex];
		return checkChar == ' ' || checkChar == '\r' || checkChar == '\t' || checkChar == '\n';
	}
	
	/**
	 * Provjerava je li neki znak na indeksu {@link #currentIndex} navodni znak.
	 * 
	 * @return <code>true</code> ako je znak navodni znak, <code>false</code> inače.
	 */
	private boolean isQuotationMark() {
		return data[currentIndex] == '\"';
	}
	/**
	 * Provjerava je li neki znak na indeksu {@link #currentIndex} operator.
	 * Operatori su svi Stringovi elementi ∈{"<", "<=", ">", ">=", "=", ">=",  "LIKE"}.
	 * 
	 * @return <code>true</code> ako je znak praznina, <code>false</code> inače.
	 */
	private boolean isOperator() {
		Character checkChar = data[currentIndex];
		if(checkChar == '<' || checkChar == '>' || checkChar == '!' || checkChar == '=') {
			if(currentIndex + 1 == data.length) {
				throw new LexerException("Improper usage of operators!");
			}
			else {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Izvlači jedan operator ako takav postoji.
	 * Operatori su svi Stringovi elementi ∈{"<", "<=", ">", ">=", "=", ">=",  "LIKE"}.
	 * 
	 * @return Sljedeći operator.
	 * @throws LexerException ako ne može naći sljedeći operator 
	 * 						  tj. ako to nije sljedeća stvar koja se može tokenizirati.
	 */
	private String extractOperator() {
		StringBuilder builder = new StringBuilder();
		builder.append(data[currentIndex]);
		if(currentIndex + 1 == data.length) {
			throw new LexerException("Improper usage of operators!");
		}
		builder.append(data[++currentIndex]);
		String check = builder.toString();
		String pattern = "<[^!><]|<=|>[^!><]|>=|=[^=!><]|!=";
		if(check.toString().matches(pattern)) {
			if(!isOperator()) {
				--currentIndex;					//Ako zadnji znak nije dio operatora smanjuje indeks
				check = check.substring(0,1);   //Miče zadnji znak
			}
			++currentIndex;
			return check;
		}
		
		if(data[--currentIndex] == 'L') {
			check = extractWord();
			if(check.equals("LIKE")) {
				return check;
			}
		}
		throw new LexerException("Improper usage of operators!");
	}
	
	/**
	 * Vraća sljedeću riječ do praznog znaka ili navodnog znaka ili operatora.
	 * 
	 * @return Sljedeća riječ za tokenizaciju.
	 */
	private String extractWord() {
		StringBuilder builder = new StringBuilder();
		for(int limit = data.length; currentIndex < limit; currentIndex++) {
			if(isBlank() || isQuotationMark() || isOperator()) {
				break;
			}
			builder.append(data[currentIndex]);
		}
		return builder.toString();
	}
	
	/**
	 * Vraća sve što je unutar navodnih znaka.
	 * 
	 * @return String u naredbi.
	 * @throws Ako String nije pravilno zatvoren.
	 */
	private String extractLiteral() {
		currentIndex++;
		String literal = extractWord();
		if(!isQuotationMark()) {
			throw new LexerException("Improper String literal quotation!");
		}
		currentIndex++;
		return literal;
	}
	/**
	 * @return {@link #token}
	 */
	public QueryToken getToken() {
		Objects.requireNonNull(token);
		return token;
	}
	
	/**
	 * Postavlja režim rada ovog leksera.
	 * 
	 * @param state {@link QueryLexerState}.
	 */
	public void setState(QueryLexerState state) {
		this.state = state;
	}
	
	
}
