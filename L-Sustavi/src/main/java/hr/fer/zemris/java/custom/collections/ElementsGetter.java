package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Parametrizirano sučelje - Predstavlja iterator nad nekom kolekcijom.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */
public interface ElementsGetter<T> {
	
	/**
	 * Provjerava postojanje sljedećeg elementa u kolekciji.
	 * 
	 * @return <code> true </code> ako sljedeći element postoji, <code> false </code> inače
	 */
	boolean hasNextElement();
	
	/**
	 * Dohvaća sljedeći element iz kolekcije.
	 * 
	 * @return <code> Object </code> sljedeći element
	 */
	T getNextElement();
	
	/**
	 * Poziva dani procesor nad svim preostalim elementima kolekcije.
	 * 
	 * @param processor Processor
	 * @throws NullPointerException ako je kao argument predan null <code>null</code>
	 */
	default void processRemaining(Processor<? super T> processor) {
		Objects.requireNonNull(processor);
		while(hasNextElement()) {
			processor.process(getNextElement());
		}
	}
	
}
