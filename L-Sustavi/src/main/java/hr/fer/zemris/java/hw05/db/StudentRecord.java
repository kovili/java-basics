package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * Klasa modelira informacije za jednog studenta.
 * 
 * @author Petar Kovač
 */
public class StudentRecord {
	/**
	 * Ime studenta.
	 */
	private String firstName;
	/**
	 * Prezime studenta.
	 */
	private String lastName;
	/**
	 * Jmbag studenta.
	 */
	private String jmbag;
	/**
	 * Završna ocjena studenta.
	 */
	private int finalGrade;
	
	/**
	 * Zadani konstruktor.
	 * 
	 * @param firstName {@link #firstName}
	 * @param lastName {@link #lastName}
	 * @param jmbag {@link #jmbag}
	 * @param finalGrade {@link #finalGrade}
	 */
	public StudentRecord(String firstName, String lastName, String jmbag, int finalGrade) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.jmbag = jmbag;
		this.finalGrade = finalGrade;
	}

	/**
	 * @return {@link #firstName}
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @return {@link #lastName}
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @return {@link #jmbag}
	 */
	public String getJmbag() {
		return jmbag;
	}
	/**
	 * @return {@link #finalGrade}
	 */
	public int getFinalGrade() {
		return finalGrade;
	}

	@Override
	public int hashCode() {
		return Objects.hash(jmbag);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof StudentRecord))
			return false;
		StudentRecord other = (StudentRecord) obj;
		return Objects.equals(jmbag, other.jmbag);
	}
	
	
	
}
