package hr.fer.zemris.java.hw05.db;

/**
 * Apstraktna strategija za dohvaćanje članskih varijabli iz klase StudentRecord.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
@FunctionalInterface
public interface IFieldValueGetter {
	/**
	 * Vraća člansku varijablu jednog {@link StudentRecord}.
	 * 
	 * @param record {@link StudentRecord}.
	 * @return Vrijednost članske varijable (ime, prezime ili jmbag).
	 */
	String get(StudentRecord record);
}
