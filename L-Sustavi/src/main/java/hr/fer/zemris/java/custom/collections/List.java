package hr.fer.zemris.java.custom.collections;

/**
 * Parametrizirano sučelje - Modelira ponašanje objekta liste.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public interface List<T> extends Collection<T> {
	
	/**
	 * Vraća element koji se nalazi na poziciji index.
	 * 
	 * @param index S kojeg želimo dohvatiti element
	 * @return Objekt na poziciji index
	 * @throws IndexOutOfBoundsException ako je (index < 0) ili (index >= size)
	 */
	T get(int index);
	
	/**
	 * Ubacuje element na danu poziciju.
	 * 
	 * @param value Objekt kojeg ubacujemo
	 * @param position Pozicija na koji će se element ubaciti u polje
	 * @throws IndexOutOfBoundsException ako je (index < 0) ili (index >= size)
	 */
	void insert(T value, int position);
	
	/**
	 * Vraća indeks na kojem se nalazi predani objekt.
	 * 
	 * @param value Objekt čiji se indeks traži
	 * @return <code> int </code> Indeks elementa
	 */
	int indexOf(Object value);
	
	/**
	 * Miče elements na poziciji indeks.
	 * 
	 * @param index Pozicija elementa kojeg se želi maknuti
	 * @throws IndexOutOfBoundsException ako je (index < 0) ili (index >= size)
	 */
	void remove(int index);
}
