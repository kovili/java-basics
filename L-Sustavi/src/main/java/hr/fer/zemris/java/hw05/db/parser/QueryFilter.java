package hr.fer.zemris.java.hw05.db.parser;

import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw05.db.ConditionalExpression;
import hr.fer.zemris.java.hw05.db.IFilter;
import hr.fer.zemris.java.hw05.db.StudentRecord;

/**
 * Klasa modelira sve apstraktne uvjete koja jedna query naredba zahtijeva.
 *
 * @author Petar Kovač
 * @version 1.0
 */
public class QueryFilter implements IFilter{
	/**
	 * Uvjeti tipa {@link ConditionalExpression}
	 */
	List<ConditionalExpression> filters;
	
	/**
	 * Zadan konstruktor.
	 * 
	 * @param filters Lista apstraktnih uvjeta.
	 * @throws NullPointerException ako je kao argument dan <code>null</code>.
	 */
	public QueryFilter(List<ConditionalExpression> filters) {
		Objects.requireNonNull(filters);
		this.filters = filters;
	}

	/**
	 * Za sve uvjete iz {@link #filters} provjerava zadovoljava li {@link StudentRecord} uvjet.
	 * 
	 * @return <code>true</code> ako {@link StudentRecord} zadovoljava sve uvjete, <code>false</code> inače.
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		for(ConditionalExpression expression : filters) {
			boolean condition = expression.getComparisonOperator().satisfied(
								expression.getFieldGetter().get(record),
								expression.getStringLiteral());
			if(!condition) {
				return false;
			}
		}
		return true;
	}

}
