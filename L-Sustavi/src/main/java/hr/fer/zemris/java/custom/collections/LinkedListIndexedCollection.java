package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;


/**
 * Klasa predstavlja dvostruko povezanu listu. Lista pamti trenutnu veličinu (količinu elemenata trenutno u listi).
 * Unošenje duplikata je dopušteno (svaki se pamti u drukčijem čvoru povezane liste), a unošenje null referenci je zabranjeno.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */
public class LinkedListIndexedCollection<T> implements List<T>{
	
	/**
	 * Broj čvorova u dvostruko povezanoj listi.
	 */
	private int size;
	
	/**
	 * Referenca na prvi {@link ListNode}. Ako je kolekcija prazna pokazuje na <code>null</code>.
	 */
	private ListNode<T> first;
	
	/**
	 * Referenca na zadnji {@link ListNode}. Ako je kolekcija prazna pokazuje na <code>null</code>.
	 */
	private ListNode<T> last;
	
	/**
	 * Broj promjena koji se dogodio u kolekciji (micanje elemenata lijevo/desno i realokacija polja).
	 * {@link LinkedListIndexedCollectionElementsGetter} uspoređuje ovaj broj sa svojim brojem kada iterira nad kolekcijom.
	 * Ako korisinik iterira nad kolekcijom, a brojevi su različiti, bacit će se iznimka.
	 */
	private long modificationCount = 0;
	/**
	 * Predstavlja jedan čvor liste.
	 * 
	 * @author Petar Kovač
	 * @version 1.0
	 */
	private static class ListNode<E> {
		/**
		 * Referenca na sljedeći {@link ListNode} u dvostruko povezanoj listi.
		 */
		private ListNode<E> next;
		/**
		 * Referenca na prijašnji {@link ListNode} u dvostruko povezanoj listi.
		 */
		private ListNode<E> previous;
		/**
		 * Vrijednost objekta sačuvanog u ovom {@link ListNode}.
		 */
		private E value;
	}
	
	/**
	 * Klasa predstavlja iterator nad dvostruko povezanom listom.
	 * 
	 * @author Petar Kovač
	 */
	private static class LinkedListIndexedCollectionElementsGetter<T> implements ElementsGetter<T> {
		/**
		 * Trenutni {@link ListNode} nad kojim iterator iterira.
		 */
		private ListNode<T> node;
		
		/**
		 * Referenca na kolekciju koja je stvorila ovaj iterator.
		 */
		private LinkedListIndexedCollection<T> parent;
		
		/**
		 * Broj promjena nad kolekcijom u trenutku stvaranja iteratora.
		 * {@link LinkedListIndexedCollectionElementsGetter} uspoređuje ovaj broj s modificationCount kada iterira nad kolekcijom.
		 * Ako korisinik iterira nad kolekcijom, a brojevi su različiti, bacit će se iznimka.
		 */
		private long savedModificationCount;
		 
		/**
		 * LinkedListIndexedCollectionElementsGetter konstruktor.
		 * 
		 * @param first Referenca na prvi ListNode
		 * @param parent Referenca na kolekciju
		 */
		public LinkedListIndexedCollectionElementsGetter(LinkedListIndexedCollection<T> parent) {
			node = parent.first;
			this.parent = parent;
			savedModificationCount = parent.modificationCount;
		}
		
		/**
		 * Provjerava postojanje sljedećeg elementa u kolekciji.
		 * 
		 * @return <code> true </code> ako sljedeći element postoji, <code> false </code> inače
		 * @throws ConcurrentModificationException Ako je iz liste maknut ili u nju dodan element
		 */
		public boolean hasNextElement() {
			if(savedModificationCount != parent.modificationCount) {
				throw new ConcurrentModificationException("The list has been drastically changed!");
			}
			return (node != null);
		}
		
		/**
		 * Dohvaća sljedeći element iz kolekcije.
		 * 
		 * @return <code> Object </code> sljedeći element
		 * @throws NoSuchElementException ako je iterator naišao na null element
		 */
		@SuppressWarnings("unchecked")
		public T getNextElement() {
			if(!hasNextElement()) {
				throw new NoSuchElementException("There are no more elements!");
			}
			Object value = node.value;
			node = node.next;
			return (T) value;
		}
	}
	/**
	 * Pretpostavljeni konstruktor za dvostruko povezanu listu. Stvara praznu listu.
	 */
	public LinkedListIndexedCollection() {
		first = last = null;
		size = 0;
	}
	/**
	 * Konstruktor koji kopira elemente druge kolekcije u ovu dvostruko povezanu listu.
	 * 
	 * @param other Kolekcija čiji se elementi kopiraju
	 */
	public LinkedListIndexedCollection(Collection<? extends T> other) {
		addAll(other);
		size = other.size();
	}
	/**
	 * Stvara primjerak iteratora nad LinkedListIndexedCollection.
	 * 
	 * @return <code> ElementsGetter </code>
	 */
	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new LinkedListIndexedCollection.LinkedListIndexedCollectionElementsGetter<T>(this);
	}
	

	/**
	 * Vraća trenutni broj elemenata kolekcije.
	 * 
	 * @return <code> int </code> Broj elemenata ove kolekcije
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Ubacuje objekt u kolekciju. Prosječno vrijeme izvođenja je O(1).
	 * 
	 * @param value Objekt kojeg želimo ubaciti
	 * @throws NullPointerException ako je predani argument jednak null
	 */
	@Override
	public void add(T value) {
		NullPointerCheck(value, "Argument must not be null!");
		if(first == null) {
			first = new ListNode<>();
			last = first;
			first.value = value;
		} else {
			last.next = new ListNode<>();
			last.next.previous = last;
			last = last.next;
			last.value = value;
		}
		modificationCount++;
		size++;
	}
	/**
	 * Vraća element koji se nalazi na poziciji index. Prosječno vrijeme izvođenja je O(n).
	 * 
	 * @param index S kojeg želimo dohvatiti element
	 * @return Objekt na poziciji index
	 * @throws IndexOutOfBoundsException ako je (index < 0) ili (index > (size - 1))
	 */
	public T get(int index) {
		IndexOutOfBoundsCheck(index, 0, size - 1, "Index is invalid!");
		ListNode<T> searchNode;
		if(size >> 1 > index) {
			searchNode = first;
			for(int i = 0, limit = index; i < limit; i++) {
				searchNode = searchNode.next;
			}
		} else {
			searchNode = last;
			for(int i = 0, limit = (size - 1) - index  ; i < limit ; i++) {
				searchNode = searchNode.previous;
			}
		}
		return searchNode.value; 
	}
	
	/**
	 * Uklanja sve elemente iz liste.
	 */
	@Override
	public void clear() {
		first = last = null;
		size = 0;
		modificationCount++;
	}
	
	/**
	 * Ubacuje element na danu poziciju.
	 * 
	 * @param value Objekt kojeg ubacujemo
	 * @param position Pozicija na koji će se element ubaciti u polje
	 * @throws IndexOutOfBoundsException ako je (position < 0) ili (position > size)
	 * @throws NullPointerException ako je argument jednak null
	 */
	public void insert(T value, int position) {
		IndexOutOfBoundsCheck(position, 0, size, "Index is invalid!");
		NullPointerCheck(value, "Argument must not be null!");
		
		if(first == null) {
			first = new ListNode<>();
			last = first;
			first.value = value;
		} else {
			ListNode<T> iterator = first;
			
			for (int i = 0; i < position; i++) {
				iterator = iterator.next;
			}
			
			ListNode<T> newNode = new ListNode<>();
			newNode.next = iterator;
			newNode.previous = iterator == null ? last : iterator.previous;
			last = iterator == null ? newNode : last;
			
			if(!(iterator == null)) {
				iterator.previous = newNode;
			}
			if(!(newNode.previous == null)) {
				newNode.previous.next = newNode;
			}
			newNode.value = value;
		}
		modificationCount++;
		size++;
	}
	
	/**
	 * Vraća indeks na kojem se nalazi predani objekt. Prosječno vrijeme izvođenja je O(n).
	 * 
	 * @param value Objekt čiji se indeks traži
	 * @return <code> int </code> Indeks elementa ako se on nalazi u kolekciji, -1 inače
	 */
	public int indexOf(Object value) {
		if(value == null) {
			return -1;
		}
		
		ListNode<T> searchNode = first;
		for(int i = 0; i < size; i++) {
			if(searchNode.value.equals(value)) {
				return i;
			}
			searchNode = searchNode.next;
		}
		return -1;
	}
	
	/**
	 * Miče elements na poziciji indeks. Prosječno vrijeme izvođenja je O(n).
	 * 
	 * @param index Pozicija elementa kojeg se želi maknuti
	 * @throws IndexOutOfBoundsException ako je (index < 0) ili (index > (size - 1))
	 */
	public void remove(int index) {
		IndexOutOfBoundsCheck(index, 0, size - 1, "Index is invalid!");
		ListNode<T> searchNode;
		
		if(size >> 1 > index) {
			searchNode = first;
			for(int i = 0, limit = index; i < limit; i++) {
				searchNode = searchNode.next;
			}
		} else {
			searchNode = last;
			for(int i = 0, limit = (size - 1) - index  ; i < limit ; i++) {
				searchNode = searchNode.previous;
			}
		}
		
		ListNode<T> deleteNode = searchNode;
		modificationCount++;
		
		if(deleteNode == first && deleteNode == last) {
			first = last = null;
			return;
		}
		
		if(!(deleteNode.next == null)) {
			deleteNode.next.previous = deleteNode.previous;
		} else {
			last = deleteNode.previous;
		}
		if(!(deleteNode.previous == null)) {
			deleteNode.previous.next = deleteNode.next;
		} else {
			first = deleteNode.next;
		}
		
		size--;
	}
	
	/**
	 * Provjerava je li lista prazna.
	 * 
	 * @return <code> true </code> ako je kolekcija prazna, inače <code> false </code>
	 */
	@Override
	public boolean isEmpty() {
		return first == null && last == null;
	}
	
	/**
	 * Provjerava prisutnost objekta u kolekciji s prosječnim vremenom izvođenja O(n).
	 * 
	 * @param value Objekt s kojim se vrši provjera
	 * @return <code> true </code> ako kolekcija sadrži objekt, inače <code> false </code>
	 */
	@Override
	public boolean contains(Object value) {
		return ((indexOf(value) != -1));
	}
	
	/**
	 * Miče jedan objekt iz liste, ako je prisutan.
	 * 
	 * @param value Objekt kojeg se želi maknuti
	 * @return <code> true </code> ako objekt postoji u kolekciji, inače false
	 */
	@Override
	public boolean remove(Object value) {
		int index = indexOf(value);
		if(index == -1) {
			return false;
		}
		remove(index);
		return true;
	}
	
	/**
	 * Daje referencu na kopiju polja elemenata kolekcije.
	 * 
	 * @return Object[] Polje kopija elemenata
	 */
	@Override
	public Object[] toArray() {
		Object []copy = new Object[size];
		ListNode<T> copyNode = first;
		for(int i = 0; i < size; i++) {
			copy[i] = copyNode.value;
			copyNode = copyNode.next;
		}
		return copy;
	}

}
