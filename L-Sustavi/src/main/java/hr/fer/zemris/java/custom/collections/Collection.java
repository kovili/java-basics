package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Parametrizirano sučelje - Predstavlja općenitu kolekciju objekata.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */

public interface Collection<T> {
	
	/**
	 * Stvara objekt tipa ElementsGetter koji vrši funkciju iteratora nad elementima kolekcije.
	 * @return <code> ElementsGetter </code>
	 */
	ElementsGetter<T> createElementsGetter();
	
	/**
	 * Na kraj kolekcije dodaje sve elemente iz kolekcije col koje prolaze test definiran Testerom tester
	 * 
	 * @param col Kolekcija čiji se elementi provjeravaju
	 * @param tester Tester koji provjerava elemente
	 * @throws NullPointerException ako je bilo koji od argumenata jednak <code>null</code>
	 */
	default void addAllSatisfying(Collection<? extends T> col, Tester<T> tester)  {
		Objects.requireNonNull(col);
		Objects.requireNonNull(tester);
		ElementsGetter<? extends T> iterator = col.createElementsGetter();
		while(iterator.hasNextElement()) {
			T obj =  iterator.getNextElement();
			if(tester.test(obj)) {
				this.add(obj);
			}
		}
	}
	
	/**
	 * Provjerava je li kolekcija prazna.
	 * 
	 * @return <code> true </code> ako je kolekcija prazna, inače <code> false </code>
	 */
	default boolean isEmpty() {
		return (this.size() == 0);
	}
	
	/**
	 * Vraća trenutni broj elemenata kolekcije.
	 * 
	 * @return <code> int </code> Broj elemenata ove kolekcije
	 */
	int size();
	
	/**
	 * Ubacuje objekt u kolekciju.
	 * 
	 * @param value Objekt kojeg želimo ubaciti
	 */
	void add(T value);
	
	/**
	 * 
	 * @param value Objekt s kojim se vrši provjera
	 * @return <code> true </code> ako kolekcija sadrži objekt, inače <code> false </code>
	 */
	
	boolean contains(Object value);
	/**
	 * Miče jedan objekt iz liste, ako je prisutan.
	 * 
	 * @param value Objekt kojeg se želi maknuti
	 * @return <code> true </code> ako objekt postoji u kolekciji, inače false
	 */
	boolean remove(Object value);
	
	/**
	 * Stvara novo polje i u njega kopira elemente kolekcije. 
	 * Veličina polja je ista kao u kolekciji.
	 * 
	 * @return Object[] Polje objekata
	 */
	Object[] toArray();
	/**
	 * Vrši operaciju definiranu procesorom nad svakim elementom kolekcije.
	 * 
	 * @param <code> processor </code> Implementacija procesora koji radi nešto nad objektom
	 * @throws NullPointerException ako je predan argument jednak <code>null</code>
	 */
	default void forEach(Processor<? super T> processor) {
		Objects.requireNonNull(processor);
		createElementsGetter().processRemaining(processor);
	}
	
	/**
	 * Dodaje sve elemente iz kolekcije other u ovu kolekciju.
	 * 
	 * @param other Kolekcija objekata koje želimo dodati
	 * @throws NullPointerException ako je predan argument jednak <code>null</code>
	 */
	default void addAll(Collection<? extends T> other) {
		Objects.requireNonNull(other);
		other.forEach(this::add);
	}
	
	/**
	 * Miče sve elemente iz ove kolekcije.
	 * 
	 */
	void clear();
	
	/**
	 * Provjera validnosti indexa.
	 * 
	 * @param index
	 * @param lowerBound
	 * @param upperBound
	 * @param message
	 * @throws IndexOutOfBoundsException ako je (index < lowerBounds) ili (index > upperBound)
	 */
	default void IndexOutOfBoundsCheck(int index, int lowerBound, int upperBound, String message) {
		if(index  < lowerBound || index > upperBound) {
			throw new IndexOutOfBoundsException(message);
		}
	}
	
	/**
	 * Provjera null vrijednosti.
	 * 
	 * @param value
	 * @param message
	 * @throws NullPointerException ako je value jednaka null
	 */
	default void NullPointerCheck(Object value, String message) {
		if(value == null) {
			throw new NullPointerException(message);
		}
	}
	
	/**
	 * Provjera validnosti argumenta.
	 * 
	 * @param value
	 * @throws IllegalArgumentExcetpion ako je vrijednost argumenta manja od 1
	 */
	default void IllegalArgumentCheck(int value) {
		if(value < 1) {
			throw new IllegalArgumentException("Number of arguments must be more than 0!");
		}
	}

}
