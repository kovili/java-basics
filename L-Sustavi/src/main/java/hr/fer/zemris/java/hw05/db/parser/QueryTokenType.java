package hr.fer.zemris.java.hw05.db.parser;

/**
 * Modelira sve moguće tipove tokena.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public enum QueryTokenType {
	/**
	 * Generira se čitanjem prve riječi nakon query ili AND operatora.
	 */
	ATTRIBUTE,
	/**
	 * Generira se čitanjem prvog znaka nakon atributa.
	 */
	OPERATOR,
	/**
	 * Generira se čitanjem String-a
	 */
	LITERAL,
	/**
	 * Generira se čitanjem AND
	 */
	AND,
	/**
	 * Generira se prolaskom kroz cijelu datoteku.
	 */
	EOF
}
