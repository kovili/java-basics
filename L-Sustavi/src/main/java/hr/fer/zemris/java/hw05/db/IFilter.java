package hr.fer.zemris.java.hw05.db;

/**
 * Apstraktna strategija za filtriranje studenata po nekom uvjetu.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
@FunctionalInterface
public interface IFilter {
	/**
	 * Metoda po kojoj se određuje je li student prihvatljiv.
	 * 
	 * @param record {@link StudentRecord}
	 * @return <code>true</code> ako je student prihvatljiv, <code>false</code> inače.
	 */
	public boolean accepts(StudentRecord record);
}
