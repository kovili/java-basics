package hr.fer.zemris.java.hw05.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.hw05.db.parser.ParserException;
import hr.fer.zemris.java.hw05.db.parser.QueryFilter;
import hr.fer.zemris.java.hw05.db.parser.QueryParser;

/**
 * Klasa modelira program baze podataka.
 * Korisnik preko konstruktora unosi ime datoteke koja sadrži podatke.
 * Trebali bi se unijeti samo podatci koji imaju jmbag, prezime, ime i ocjenu tim redom.
 * Jmbagovi koji su već unešeni ne smiju ponovo biti unešeni.
 * Završne ocjene mogu biti samo ∈{1, 2, 3, 4, 5}.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */
public class StudentDB {
	
	/**
	 * Glavna metoda za pokretanje i rad s bazom podataka.
	 */
	public static void main(String[] args) {
		StudentDB database = new StudentDB("./src/main/resources/database.txt");
		Scanner input = new Scanner(System.in);
		database.query(input);
		
	}
	/**
	 * Baza podataka studenata koja se inicijalizira tek kada se uspješno pročitaju svi podatci.
	 */
	private StudentDatabase database;
	
	/**
	 * Zadani konstruktor koji poziva metodu za inicijalizaciju.
	 * 
	 * @param path Put do datoteke s podatcima.
	 */
	public StudentDB(String path) {
		load(path);
	}
	
	/**
	 * Glavna metoda za inicijalizaciju. 
	 * Čita sve retke datoteke. Ako je svaki redak korektan, dodaje ih u bazu podataka.
	 * 
	 * @param path Put do datoteke s podatcima.
	 */
	private void load(String path) {
		List<String> lines;
		HashSet<String> jmbags = new HashSet<>();
		try {
			lines = Files.readAllLines(
					 Paths.get(path),
					 StandardCharsets.UTF_8
					);
			
			String []data = new String[lines.size()];
			String []info;
			for(int i = 0, limit = lines.size(); i < limit; i++) {
				data[i] = lines.get(i);
				info = data[i].strip().split("\\s+");
				checkData(info);
				if(jmbags.contains(info[0])) {
					System.out.println("Duplicate jmbag entered.");
					System.exit(0);
				}
				jmbags.add(data[i]);
			}
			database = new StudentDatabase(data);
		} catch (IOException e) {
			System.out.println("File can't be read!");
		}
	}
	
	/**
	 * Metoda kojom se program 'pokreće'.
	 * Svaka naredba mora početi ključnom riječi 'query'.
	 * Prima naredbe i interpretira ih te prikazuje rezultate.
	 * Javlja ako je unos netočan.
	 * Za kraj treba se napisati 'exit'.
	 * 
	 * @param input {@link Scanner} za čitanje unosa sa standardnog unosa.
	 */
	public void query(Scanner input) {
		List<StudentRecord> records;
		String query;
		while(true) {
			System.out.printf("> ");
			if(input.hasNextLine()) {
				query = input.nextLine();
				if(query.equals("exit")) {
					System.out.println("Goodbye!");
					break;
				}
				if(query.matches("query.*")) {
					query = query.replaceFirst("query", "");
					try {
						records = getFilteredRecords(query);
						List<String> output = RecordFormatter.format(records);
						output.forEach(System.out::println);
						if(records.contains(null)) {
							System.out.println("Records selected: 0");
						} else {
							System.out.println("Records selected: " +  records.size());
						}
						System.out.println("");
					} catch(ParserException e) {
						System.out.println(e.getMessage());
					}
				} else {
					System.out.println("Invalid query! Must contain keyword query or keyword exit!");
				}
			}
		}
		input.close();
	}
	
	/**
	 * Nalazi sve primjerke {@link StudentRecord} koji zadovoljavaju uvjete navedene query naredbom.
	 * 
	 * @param query Naredba
	 * @return Lista prihvaćenih primjeraka {@link StudentRecord}.
	 */
	private List<StudentRecord> getFilteredRecords(String query) {
		QueryParser parser = new QueryParser(query);
		List<StudentRecord> records = new ArrayList<>();
		
		if(parser.isDirectQuery()) {
			records.add(database.forJMBAG(parser.getQueriedJMBAG()));
			return records;
		} else {
			return database.filter(new QueryFilter(parser.getQuery()));
		}
	}
	
	/**
	 * Provjerava je li jedna linija datoteke pravilno napisana.
	 * Pravilno je napisana ako ima više od 3 String-a,
	 * ako nema ocjenu izvan dopuštenog područja 
	 * i najvažnije, ako pripadni jmbag nije već unesen.
	 * 
	 * @param entry Linija teksta razlomljena na riječi.
	 */
	private void checkData(String []entry) {
		if(entry.length < 4) {
			System.out.println("Line must have atleast 4 elements!");
			System.exit(0);
		}
		int grade = Integer.parseInt(entry[entry.length - 1]);
		if(grade < 1 || grade > 5) {
			System.out.println("Out of range grade entered!");
			System.exit(0);
		}
	}
}
