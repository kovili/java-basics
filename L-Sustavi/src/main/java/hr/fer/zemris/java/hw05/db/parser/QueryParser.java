package hr.fer.zemris.java.hw05.db.parser;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.hw05.db.ComparisonOperators;
import hr.fer.zemris.java.hw05.db.ConditionalExpression;
import hr.fer.zemris.java.hw05.db.FieldValueGetters;
import hr.fer.zemris.java.hw05.db.IComparisonOperator;
import hr.fer.zemris.java.hw05.db.IFieldValueGetter;
/**
 * Parser za query naredbe.
 *
 * @author Petar Kovač
 * @version 1.0
 */
public class QueryParser {
	/**
	 * Trenutni token kojeg je parser dobio od leksera.
	 */
	private QueryToken currentToken;
	/**
	 * Lekser kojeg ovaj parser koristi za generaciju tokena.
	 */
	private QueryLexer lexer;
	/**
	 * Lista apstraktnih uvjeta koje opisuju query naredbu.
	 */
	private List<ConditionalExpression> query;
	/**
	 * Stog za čuvanje podataka tijekom parsiranja.
	 */
	private ObjectStack<String> stack;
	
	/**
	 * Zadani konstruktor koji prima naredbu poziva metodu za parsiranje.
	 * 
	 * @param query Naredba.
	 * @throws NullPointerException ako je naredba jednaka <code>null</code>.
	 * @throws ParserException ako se dogodila greška tijekom parsiranja.
	 */
	public QueryParser(String query) {
		Objects.requireNonNull(query);
		lexer = new QueryLexer(query);
		lexer.setState(QueryLexerState.WORD);
		this.query = new ArrayList<>();
		stack = new ObjectStack<>();
		try {
			parse();
		} catch(LexerException e) {
			throw new ParserException("Invalid query!");
		}
	}
	
	/**
	 * Parsira unešenu query naredbu.
	 * Prestaje s parsiranjem kada dođe do EOF.
	 * @throws ParserException ako se dogodila greška tijekom parsiranja.
	 */
	private void parse() {
		currentToken = lexer.nextToken();
		if(currentToken.getType() == QueryTokenType.EOF) {
			if(query.isEmpty()) {
				throw new ParserException("Invalid query!");
			}
			return;
		}
		
		if(currentToken.getType() == QueryTokenType.ATTRIBUTE) {
			lexer.setState(QueryLexerState.OPERATOR);
			stack.push(currentToken.getValue());
			parse();
			return;
		}
		
		if(currentToken.getType() == QueryTokenType.OPERATOR) {
			lexer.setState(QueryLexerState.LITERAL);
			stack.push(currentToken.getValue());
			parse();
			return;
		}
		
		if(currentToken.getType() == QueryTokenType.LITERAL) {
			lexer.setState(QueryLexerState.AND);
			stack.push(currentToken.getValue());
			extractConditionalExpression();
			parse();
			return;
		}
		
		if(currentToken.getType() == QueryTokenType.AND) {
			lexer.setState(QueryLexerState.WORD);
			parse();
			return;
		}
		
		throw new ParserException("Invalid query!");
	}
	
	/**
	 * Metoda u listu apstraktnih naredbi dodaje jednu koju je parser uspio parsirati.
	 */
	private void extractConditionalExpression() {
		String literal = stack.pop();
		String operator = stack.pop();
		String attribute = stack.pop();
		
		ConditionalExpression exp = new ConditionalExpression
										(findGetter(attribute),
										literal, 
										findOperator(operator));
		query.add(exp);
	}
	
	/**
	 * Metoda vraća pripadnu konkretnu strategiju za neki atribut.
	 * 
	 * @param attribute firstName ili lastName ili jmbag
	 * @return Implementaciju {@link IFieldValueGetter} - konkretnu strategiju.
	 * @throws ParserException ako je unesen neprepoznatljiv atribut.
	 */
	private static IFieldValueGetter findGetter(String attribute) {
		if(attribute.equals("firstName")) {
			return FieldValueGetters.FIRST_NAME;
		}
		
		if(attribute.equals("lastName")) {
			return FieldValueGetters.LAST_NAME;
		}
		
		if(attribute.equals("jmbag")) {
			return FieldValueGetters.JMBAG;
		}
		throw new ParserException("Invalid attribute in query!");
	}
	
	/**
	 * Metoda vraća pripadnu konrektnu strategiju za neki operator.
	 * 
	 * @param operator koji je ∈{"<", "<=", ">", ">=", "=", ">=",  "LIKE"}.
	 * @return Implementaciju {@link IComparisonOperator} - konkretnu strategiju.
	 * @throws ParserException ako operator nije prepoznat.
	 */
	private static IComparisonOperator findOperator(String operator) {
		if(operator.equals("LIKE")) {
			return ComparisonOperators.LIKE;
		}
		if(operator.equals("<")) {
			return ComparisonOperators.LESS;
		}
		if(operator.equals("<=")) {
			return ComparisonOperators.LESS_OR_EQUALS;
		}
		if(operator.equals(">")) {
			return ComparisonOperators.GREATER;
		}
		if(operator.equals(">=")) {
			return ComparisonOperators.GREATER_OR_EQUALS;
		}
		if(operator.equals("=")) {
			return ComparisonOperators.EQUALS;
		}
		if(operator.equals("!=")) {
			return ComparisonOperators.NOT_EQUALS;
		}
		throw new ParserException("Invalid operator in query!");
	}
	
	/**
	 * Provjerava je li unešen samo jedan uvjet tipa jmbag = "xx..."
	 * 
	 * @return <code>true</code> ako je unesen direktan upit, <code>false</code> inače.
	 */
	public boolean isDirectQuery() {
		if(query.size() == 1){
			if(query.get(0).getFieldGetter() == FieldValueGetters.JMBAG) {
				if(query.get(0).getComparisonOperator() == ComparisonOperators.EQUALS) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Vraća JMBAG vezan za direktan upit ovog parsera.
	 * 
	 * @return JMBAG
	 * @throws IllegalStateException ako naredba nije bila direktan upit.
	 */
	public String getQueriedJMBAG() {
		if(!isDirectQuery()) {
			throw new IllegalStateException("Cannot be called over indirect queries!");
		}
		return query.get(0).getStringLiteral();
	}
	
	/**
	 * @return {@link #query}
	 */
	public List<ConditionalExpression> getQuery() {
		return query;
	}
}
