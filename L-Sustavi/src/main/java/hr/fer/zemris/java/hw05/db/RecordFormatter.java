package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa koja sadrži metode za formatiranje teksta baze podataka.
 *
 * @author Petar Kovač
 * @version 1.0
 */
public class RecordFormatter {
	/**
	 * Širina znaka ocjene.
	 */
	private static final int GRADE_WIDTH = 1;
	/**
	 * Širina obruba jedne ćelije.
	 */
	private static final int BWIDTH = 1;
	
	/**
	 * Metoda za formatiranje.
	 * Širina svakog stupca će biti velika koliko je i širina najduljeg primjerka atributa.
	 * 
	 * @param records {@link StudentRecord} zapisi o studentima.
	 * @return Formatirana lista Stringova.
	 */
	public static List<String> format(List<StudentRecord> records) {
		List<String> formatted = new ArrayList<>();
		if(records.isEmpty() || records.contains(null)) {
			formatted.add("");
			return formatted;
		}
		int jmbagWidth = 0;
		int firstNameWidth = 0;
		int lastNameWidth = 0;
		
		if(records.size() != 0) {
			jmbagWidth = records.stream().mapToInt((s) -> s.getJmbag().length()).max().getAsInt();

			firstNameWidth = records.stream().mapToInt((s) -> s.getFirstName().length()).max().getAsInt();

			lastNameWidth = records.stream().mapToInt((s) -> s.getLastName().length()).max().getAsInt();
		}

		
		String top = "+".concat("=".repeat(jmbagWidth + 2 * BWIDTH)).concat("+");
		top = top.concat("=".repeat(lastNameWidth + 2 * BWIDTH)).concat("+");
		top = top.concat("=".repeat(firstNameWidth + 2 * BWIDTH)).concat("+");
		top = top.concat("=".repeat(GRADE_WIDTH + 2 * BWIDTH)).concat("+");
		
		formatted.add(top);
		for(StudentRecord record : records) {
			formatted.add(lineFormat(record, jmbagWidth, lastNameWidth, firstNameWidth));
		}
		formatted.add(top);
		
		return formatted;		
	}
	/**
	 * Metoda koja formatira jedan redak baze podataka.
	 * 
	 * @param record Zapis o jednom studentu.
	 * @param jmbagWidth Najdulji jmbag.
	 * @param lastNameWidth Najdulje prezime.
	 * @param firstNameWidth Najdulje ime.
	 * @return Formatirani zapis jednog retka baze podataka.
	 */
	private static String lineFormat(StudentRecord record, int jmbagWidth, int lastNameWidth, int firstNameWidth) {
		int jmbagBlankW = jmbagWidth - record.getJmbag().length() + BWIDTH;
		int firstNameBlankW = firstNameWidth - record.getFirstName().length() + BWIDTH;
		int lastNameBlankW = lastNameWidth - record.getLastName().length() + BWIDTH;
		String line ="|";
		line = line.concat(" ").concat(record.getJmbag()).concat(" ".repeat(jmbagBlankW));
		line = line.concat("|").concat(" ").concat(record.getLastName()).concat(" ".repeat(lastNameBlankW));
		line = line.concat("|").concat(" ").concat(record.getFirstName()).concat(" ".repeat(firstNameBlankW));
		line = line.concat("|").concat(" ").concat(Integer.toString(record.getFinalGrade()));
		line = line.concat(" |");
		return line;
	}
	
	
	
}
