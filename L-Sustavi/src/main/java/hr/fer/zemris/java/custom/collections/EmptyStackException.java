package hr.fer.zemris.java.custom.collections;

/**
 * Razred predstavlja iznimku koju se baca pozivom funkcije pop() kada je stog prazan.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class EmptyStackException extends RuntimeException{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmptyStackException() {
	}

	public EmptyStackException(String message) {
		super(message);
	}
	
	public EmptyStackException(Throwable cause) {
		super(cause);
	}
	
	public EmptyStackException(String message, Throwable cause) {
		super(message, cause);
	}
}
