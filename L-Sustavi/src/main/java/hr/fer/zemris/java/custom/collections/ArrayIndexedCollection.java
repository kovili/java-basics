package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Klasa predstvalja kolekciju koju se indeksira kao polje. Polje pamti trenutnu veličinu (količinu elemenata trenutno u polju).
 * Unošenje duplikata je dopušteno, a unošenje null referenci je zabranjeno.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */

public class ArrayIndexedCollection<T> implements List<T> {

	/**
	 * Definira početnu vrijednost veličine kolekcije. Ova vrijednost će biti korištena u zadanom konstruktoru.
	 */
	private static final int DEFAULT_ARRAY_SIZE = 16;
	
	/**
	 * Definira broj kojim se množi veličina alociranog prostora u slučaju realokacije.
	 */
	private static final int RESIZE_FACTOR = 2;
	
	/**
	 * Broj elemenata koji su trenutno u kolekciji, a nisu null.
	 */
	private int size;
	
	/**
	 * Polje elemenata kolekcije.
	 */
	private T[] elements;
	
	/**
	 * Broj promjena koji se dogodio u kolekciji (micanje elemenata lijevo/desno i realokacija polja).
	 * {@link ArrayIndexedCollectionElementsGetter} uspoređuje ovaj broj sa svojim brojem kada iterira nad kolekcijom.
	 * Ako korisinik iterira nad kolekcijom, a brojevi su različiti, bacit će se iznimka.
	 */
	private long modificationCount = 0;
	
	/**
	 * Klasa predstavlja iterator nad poljem elemenata ArrayIndexedCollection.
	 * @author Petar Kovač
	 *
	 */
	private static class ArrayIndexedCollectionElementsGetter<T> implements ElementsGetter<T> {
		/**
		 * Trenutna pozicija iteratora u polju ove kolekcije.
		 */
		private int index;
		
		/**
		 * Referenca na kolekciju koja je stvorila ovaj iterator.
		 */
		private ArrayIndexedCollection<T> parent;
		
		/**
		 * Broj promjena nad kolekcijom u trenutku stvaranja iteratora.
		 * {@link ArrayIndexedCollectionElementsGetter} uspoređuje ovaj broj s modificationCount kada iterira nad kolekcijom.
		 * Ako korisinik iterira nad kolekcijom, a brojevi su različiti, bacit će se iznimka.
		 */
		private long savedModificationCount;
		
		/**
		 * ArrayIndexedCollectionElementsGetter konstruktor.
		 * 
		 * @param parent Primjerak ArrayIndexedCollection nad kojim će se vršiti iteracija.
		 */
		public ArrayIndexedCollectionElementsGetter(ArrayIndexedCollection<T> parent) {
			this.parent = parent;
			index = 0;
			savedModificationCount = parent.modificationCount;
		}
		/**
		 * Provjerava postojanje sljedećeg elementa u kolekciji.
		 * 
		 * @return <code> true </code> ako sljedeći element postoji, <code> false </code> inače
		 * @throws ConcurrentModificationException Ako je polje bilo realocirano ili ako s članovi bili pomaknuti (lijevo ili desno)
		 */
		public boolean hasNextElement() {
			if(parent.modificationCount != savedModificationCount) {
				throw new ConcurrentModificationException("The array has been drastically changed!");
			}
			return (index < parent.size);
		}
		
		/**
		 * Dohvaća sljedeći element iz kolekcije.
		 * 
		 * @return <code> Object </code> sljedeći element
		 * @throws NoSuchElementException ako je iterator prešao preko kraja liste (index >= size)
		 */
		public T getNextElement() {
			if(!hasNextElement()) {
				throw new NoSuchElementException("There is no next element!");
			}
			return parent.elements[index++];
		}
	}
	
	/**
	 * Zadan konstruktor. Veličina kolekcije će biti jednaka DEFAULT_ARRAY_SIZE.
	 */
	public ArrayIndexedCollection() {
		this(DEFAULT_ARRAY_SIZE);
	}
	/**
	 * Stvara kolekciju veličine initialCapacity.
	 * 
	 * @param initialCapacity Željena veličina kolekcije
	 * @throws IllegalArgumentException ako je initialCapacity manji od 1
	 */
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(int initialCapacity) {
		IllegalArgumentCheck(initialCapacity);
		elements = (T [])new Object[initialCapacity];
		size = 0;
	}
	
	/**
	 * Stvara kolekciju s kopiranim elementima iz kolekcije other.
	 * 
	 * @param other Kolekcija koja će se kopirati
	 */
	public ArrayIndexedCollection(Collection<? extends T> other) {
		this(other, DEFAULT_ARRAY_SIZE);
	}
	
	/**
	 * Stvara kolekciju s kopiranim elementima iz kolekcije other, 
	 * ako je initialCapacity veći ili jednak od veličine kolekcije other, 
	 * inače stvara kolekciju veličine other.size().
	 * 
	 * @param other Kolekcija koja će se kopirati
	 * @param initialCapacity Početna veličina kolekcije
	 * @throws NullPointerException ako je kolekcija 'other' null pointer
	 * @throws IllegalArgumentException ako je initialCapacity manji od 1
	 */
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(Collection<? extends T> other, int initialCapacity) {
		Objects.requireNonNull(other);
		IllegalArgumentCheck(initialCapacity);
		
		elements = (T[]) new Object[initialCapacity < other.size() ? other.size() : initialCapacity];
		
		this.addAll(other);
		size = other.size();
	}
	
	/**
	 * Stvara primjerak iteratora nad ArrayListIndexedCollection.
	 * 
	 * @return <code> ElementsGetter </code>
	 */
	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new ArrayIndexedCollectionElementsGetter<T>(this);
	}
	

	/**
	 * Vraća trenutni broj elemenata kolekcije.
	 * 
	 * @return <code> int </code> Broj elemenata ove kolekcije
	 */
	@Override
	public int size() {
		return size;
	}
	
	/**
	 * Ubacuje objekt u kolekciju. Udvostručuje kapacitet kolekcije, ako nema dovoljno mjesta za ubacivanje.
	 * 
	 *  @param value Objekt kojeg želimo ubaciti
	 *  @throws NullPointerException ako je predani argument jednak null
	 */
	@Override
	public void add(T value) {
		Objects.requireNonNull(value);
		if(!(size < elements.length)) {
			elements = Arrays.copyOf(elements, elements.length * RESIZE_FACTOR);
			modificationCount++;
		}
		elements[size++] = value;
	}
	/**
	 * Provjerava prisutnost objekta u kolekciji s prosječnim vremenom izvođenja O(n).
	 * 
	 * @param value Objekt s kojim se vrši provjera
	 * @return <code> true </code> ako kolekcija sadrži objekt, inače <code> false </code>
	 * @throws NullPointerException ako su neka vrijednost u kolekciji ili argument jednaki null
	 */
	@Override
	public boolean contains(Object value) {
		return indexOf(value) >= 0;
	}
	
	/**
	 * Miče jedan objekt iz liste, ako je prisutan.
	 * 
	 * @param value Objekt kojeg se želi maknuti
	 * @return <code> true </code> ako objekt postoji u kolekciji, inače false
	 * @throws NullPointerException ako je predani argument jednak null
	 */
	@Override
	public boolean remove(Object value) {
		if(value == null) {
			return false;
		}
		for(int i = 0; i < size; i++) {
			if(elements[i].equals(value)) {
				if(size != i + 1) {
					System.arraycopy(elements, (i + 1), elements, i, elements.length - (i + 1));
					modificationCount++;
				}
				elements[--size] = null;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Daje referencu na kopiju polja elemenata kolekcije.
	 * 
	 * @return Object[] Polje kopija elemenata
	 */
	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		System.arraycopy(elements, 0, array, 0, size);
		return array;
	}
	
	
	/**
	 * Uklanja sve elemente iz polja. Polje ostaje na trenutnoj veličini.
	 */
	@Override
	public void clear() {
		modificationCount++;
		for(int i = 0; i < size; i++) {
			elements[i] = null;
		}
		size = 0;
	}
	
	/**
	 * Vraća element koji se nalazi na poziciji index. Prosječno vrijeme izvođenja je O(1).
	 * 
	 * @param index Pozicija s koje se uzima element
	 * @return <code> Object </code> Element s pozicije index
	 * @throws IndexOutOfBoundsException Ako je (index < 0) ili (index > (size - 1));
	 */
	public T get(int index) {
		IndexOutOfBoundsCheck(index, 0, size - 1, "Index is invalid!");
		return elements[index];
	}
	
	/**
	 * Ubacuje element na danu poziciju.
	 * 
	 * @param value Objekt koji ubacujemo
	 * @param position Pozicija na koji će se element ubaciti u polje
	 * @throws IndexOutOfBoundsException ako je (position < 0) ili (position > size)
	 * @throws NullPointerException ako je argument jednak <code>null</code>
	 */
	public void insert(T value, int position) {
		IndexOutOfBoundsCheck(position, 0, size, "Index is invalid!");
		Objects.requireNonNull(value);
		if(position == size) {
			add(value);
			return;
		}
		if(!(size < elements.length)) {
			elements = Arrays.copyOf(elements, elements.length * RESIZE_FACTOR);
			modificationCount++;
		}
		System.arraycopy(elements, position, elements, (position + 1), size - position);
		elements[position] = value;
		modificationCount++;
		size++;

	}
	
	/**
	 * Vraća indeks na kojoj se nalazi predani objekt. Prosječno vrijeme izvođenja je O(n).
	 * 
	 * @param value Objekt čiji se indeks traži
	 * @return <code> int </code> Indeks elementa ako se on nalazi u kolekciji, -1 inače
	 */
	public int indexOf(Object value) {
		if(value == null) {
			return -1;
		}
		for(int i = 0; i < size; i++) {
			if(elements[i].equals(value)) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Miče elements na poziciji indeks.
	 * 
	 * @param index Pozicija elementa kojeg se želi maknuti
	 * @throws IndexOutOfBoundsException ako je (index < 0) ili (index > (size - 1))
	 */
	public void remove(int index) {
		IndexOutOfBoundsCheck(index, 0, size - 1, "Index is invalid!");
		if(!(index == (size - 1))) {
			System.arraycopy(elements, (index + 1), elements, index, size - (index + 1));
			modificationCount++;
		}
		elements[--size] = null;
		
	}
	
	public int capacity() {
		return elements.length;
	}
	
}
