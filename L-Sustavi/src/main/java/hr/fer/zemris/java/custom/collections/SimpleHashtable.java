package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;


/**
 * Parametrizirana klasa koja predstavlja jednostavnu tablicu raspršenog adresiranja.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 * @param <K> Ključ zapisa
 * @param <V> Vrijednost zapisa
 */
public class SimpleHashtable <K, V> implements Iterable<SimpleHashtable.TableEntry<K, V>>{
	/**
	 * Konstanta koja određuje početni broj pretinaca za zadani konstruktor.
	 */
	private static final int DEFAULT_SIZE = 16;
	
	/**
	 * Konstanta koja određuje najveću popunjenost tablice prije nego što se dogodi realokacija.
	 */
	private static final double MAX_CAPACITY = 0.75;
	
	/**
	 * Konstanta koja određuje za koji faktor se kapacitet polja tablice uvećava pri realokaciji.
	 */
	private static final int CAPACITY_INCREASE_FACTOR = 2;
	/**
	 * Polje parametriziranih zapisa tipa ključ->vrijednost.
	 */
	private TableEntry<K,V> []table;
	/**
	 * Broj elemenata u tablici raspršenog adresiranja.
	 */
	private int size;
	
	/**
	 * Broj popunjenih pretinaca. 
	 * Ako broj popunjenih pretinaca postane jednak ili veći od 75% kapaciteta polja, dogoditi će se realokacija.
	 */
	private int filledSlots;
	
	/**
	 * Broj modifikacija koje su načinjene nad tablicom raspršenog adresiranja.
	 * Pod modifikacije ubrajamo: put(), remove(), clear().
	 */
	private long modificationCount;
	
	/**
	 * Zadani konstruktor. Koristi početnu vrijednost DEFAULT_SIZE.
	 */
	public SimpleHashtable() {
		this(DEFAULT_SIZE);
	}
	/**
	 * Konstruktor u kojem korisnik može dati željenu veličinu tablice.
	 * Broj koji se zapravo odabire je prva sljedeća potencija broja 2 (npr. za unos 30 će veličina tablice biti 32).
	 *
	 * @param capacity Željena veličina tablice
	 * @throws IllegalArgumentException ako je unesena veličina manja od 1
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int capacity) {
		if(capacity <= 0) {
			throw new IllegalArgumentException("Size of hashtable must be strictly positive!");
		}
		table = new TableEntry[getFirstPowerOf2EqualOrGreaterThan(capacity)];
	}
	
	/**
	 * Klasa predstavlja jedan zapis unutar tablice raspršenog adresiranja.
	 * Moguće je stvoriti primjerak zapisa bez primjerka tablice.
	 * 
	 * @author Petar Kovač
	 * @version 1.0
	 *
	 * @param <T> Ključ zapisa
	 * @param <E> Vrijednost zapisa
	 */
	public static class TableEntry<T, E> {
		/**
		 * Ključ za koji se veže vrijednost. Ne smije biti null.
		 */
		private T key;
		/**
		 * Vrijednost vezana za neki ključ.
		 */
		private E value;
		/**
		 * Referenca na sljedeći zapis.
		 */
		private TableEntry<T, E> next;
		
		/**
		 * Konstruktor koji prima ključ i vrijednost, gdje ključ ne smije, a vrijednost smije biti null.
		 * 
		 * @param key Ključ zapisa
		 * @param value Vrijednost zapisa
		 * @throws NullPointerException ako je za vrijednost key dan <code>null</code>
		 */
		public TableEntry(T key, E value) {
			Objects.requireNonNull(key);
			this.key = key;
			this.value = value;
		}
		/**
		 * Vraća vrijednost ovog zapisa.
		 * 
		 * @return <E> Vrijednost zapisa
		 */
		public E getValue() {
			return value;
		}
		/**
		 * Mijenja vrijednost ovog zapisa na danu vrijednost.
		 * 
		 * @param value Vrijednost zapisa
		 */
		public void setValue(E value) {
			this.value = value;
		}
		/**
		 * Vraća vrijednost ključa ovog zapisa.
		 * 
		 * @return <T> Ključ zapisa
		 */
		public T getKey() {
			return key;
		}	
	}
	
	/**
	 * Računa indeks na koji se treba ubaciti zapis s danim ključem u tablicu raspršenog adresiranja.
	 * 
	 * @param key Ključ zapisa
	 * @return Indeks u koji se treba ubaciti ključ u rasponu [0, kapacitet]
	 */
	private int getHash(Object key) {
		return Math.abs(key.hashCode()) % table.length;
	}
	
	/**
	 * Nalazi zapis na danom indeksu s danim ključem.
	 * 
	 * @param key Ključ zapisa
	 * @param index Indeks u tablici raspršenog adresiranja
	 * @return {@link TableEntry} ako je nađen zapis s jednakim ključem, inače <code>null</code>
	 */
	private TableEntry<K,V> findNode(Object key, int index) {
		TableEntry<K,V> entry = table[index];
		while(entry != null) {
			if(entry.key.equals(key)) {
				return entry;
			}
			entry = entry.next;
		}
		return null;
	}
	
	/**
	 * Stavlja jedan zapis s ključem key i vrijednošću value, ako zapis s takvim ključem ne postoji u tablici.
	 * Ako zapis postoji, samo mijenja vrijednost zapisa na value.
	 * 
	 * @param key Ključ zapisa
	 * @param value Vrijednost zapisa
	 * @throws NullPointerException ako je vrijednost danog ključa jednaka <code>null</code>
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);
		int index = getHash(key);
		TableEntry<K, V> entry = findNode(key, index);
		if(entry != null) {
			entry.value = value;
			return;
		}
		
		entry = (TableEntry<K, V>) table[index];

		while(entry != null) {
			if(entry.next == null) {
				entry.next = new TableEntry<>(key, value);
				size++;
				modificationCount++;
				return;
			}
			entry = entry.next;
		}
		
		filledSlots++;
		table[index] = new TableEntry<>(key, value);
		if(isSaturated()) {
			reallocate();
			return;
		}
		modificationCount++;
		size++;
		return;
		
	}
	
	/**
	 * Vraća vrijednost koja je vezana uz ključ key.
	 * 
	 * @param key Ključ zapisa
	 * @return <V> Vrijednost zapisa ako ona postoji, inače <code>null</code>
	 */
	public V get(K key) {
		if(key == null) {
			return null;
		}
		
		TableEntry<K, V> entry = findNode(key, getHash(key));
		if(entry == null) {
			return null;
		}
		return entry.value;
	}
	
	/**
	 * Vraća broj elemenata u ovoj tablici raspršenog adresiranja.
	 * 
	 * @return Broj elemenata
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Provjerava postoji li zapis s danim ključem u tablici raspršenog adresiranja.
	 * SLoženost O(1).
	 * 
	 * @param key Ključ zapisa
	 * @return <code>true</code> ako zapis postoji, inače <code>false</code>
	 */
	public boolean containsKey(Object key) {
		if(key == null) {
			return false;
		}
		if(findNode(key, getHash(key)) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * Provjerava sadrži li tablica zapis s danom vrijednošću.
	 * Složenost O(n).
	 * 
	 * @param value Vrijednost zapisa
	 * @return <code>true</code> ako se zapis s danom vrijednošću nalazi u tablici raspršenog adresiranja, <code>false</code> inače
	 */
	public boolean containsValue(Object value) {
		for(int i = 0, limit = table.length; i < limit; i++) {
			TableEntry<K,V> entry = table[i];
			while(entry != null) {
				if(value == entry.value) {
					return true;
				}
				if(entry.value != null) {
					if(entry.value.equals(value)) {
						return true;
					}
				}
				entry = entry.next;
			}
		}
		return false;
	}
	/**
	 * Miče zapis čiji je ključ jednak key iz tablice raspršenog adresiranja.
	 * 
	 * @param key Ključ zapisa
	 */
	public void remove(Object key) {
		if(key == null) {
			return;
		}
		int index = getHash(key);
		TableEntry<K, V> entry = table[index];
		if(entry == null) {
			return;
		}
		if(entry.key.equals(key)) {
			table[index] = entry.next;
			if(table[index] == null) {
				filledSlots--;
			}
			modificationCount++;
			size--;
			return;
		}
		while(entry.next != null) {
			if(entry.next.key.equals(key)) {
				entry.next = entry.next.next;
				modificationCount++;
				size--;
				return;
			}
			entry = entry.next;
		}
	}
	
	/**
	 * Određuje je li tablica raspršenog adresiranja prazna.
	 * 
	 * @return <code>true</code> ako tablica nema zapisa, inače <code>false</code>
	 */
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * Računa je li broj popunjenih pretinaca prešao 75% tablice.
	 * @return <code>true</code> ako je broj popunjenih pretinaca veći od 75% kapaciteta, inače <code>false</code>
	 */
	private boolean isSaturated() {
		return (double) filledSlots/table.length >= MAX_CAPACITY;
	}
	
	/**
	 * Vraća prikaz svih zapisa u tablici u obliku "[key1=value1, key2=value2, key3=value3]"
	 */
	@Override
	public String toString() {
		TableEntry<K, V> entry;
		StringBuilder builder = new StringBuilder("[");
		for(int i = 0, limit = table.length; i < limit; i++) {
			entry = table[i];
			while(entry != null) {
				builder.append(entry.key.toString()).append("=").append(entry.value).append(", ");
				entry = entry.next;
			}
		}
		String result = builder.substring(0, builder.length() - 2);
		result = result.concat("]");
		return result;
	}
	
	/**
	 * Miče sve reference na zapise u ovoj tablici raspršenog adresiranja i postavlja broj elemenata na 0.
	 */
	public void clear() {
		Arrays.fill(this.table, null);
		modificationCount++;
		filledSlots = size = 0;
	}
	/**
	 * Realocira kapacitet tablice raspršenog adresiranja tako da ga uveća za {@code CAPACITY_INCREASE_FACTOR}.
	 * Događa se kad tablica dosegne broj popunjenih pretinaca jednak ili veći 75% od kapaciteta tablice.
	 * Ako se ne dogodi realokacija, dolazi do nepovoljne računske kompleksnosti.
	 */
	@SuppressWarnings("unchecked")
	private void reallocate() {
		TableEntry<K, V> []oldEntries = table;
		table = new TableEntry[table.length * CAPACITY_INCREASE_FACTOR];
		TableEntry<K, V> entry;
		filledSlots = size = 0;
		modificationCount++;
		
		for(int i = 0, limit = oldEntries.length; i < limit; i++) {
			entry = oldEntries[i];
			while(entry != null) {
				put(entry.key, entry.value);
				entry = entry.next;
			}
		}
	}
	/**
	 * Vraća kapacitet tablice, koristi se primarno za testiranje.
	 * @return Kapacitet tablice.
	 */
	public int capacity() {
		return table.length;
	}
	
	/**
	 * Klasa predstavlja iterator nad tablicom raspršenog adresiranja.
	 * 
	 * @author Petar Kovač
	 * @version 1.0
	 *
	 */
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K, V>> {
		/**
		 * Trenutna pozicija iteratora u tablici raspršenog adresiranja.
		 */
		private int currentIndex;
		/**
		 * Broj elemenata koji je iterator obradio.
		 */
		private int countElements;
		/**
		 * Zadnji zapis izbačen metodom next.
		 */
		private TableEntry<K, V> currentEntry;
		
		/**
		 * Broj promjena koji se dogodio nad tablicom raspršenog adresiranja do trenutka stvaranja ovog primjerka iteratora.
		 */
		private long iteratorModificationCount;
		
		/**
		 * Konstruktor za iterator nad tablicom raspršenog adresiranja.
		 * Pamti broj trenutnih promjena nad tablicom.
		 */
		public IteratorImpl() {
			super();
			iteratorModificationCount = modificationCount;
		}
		/**
		 * @throws ConcurrentModificationException Ako se dodavalo ili oduzimalo elemente tablice raspršenog adresiranja bez korištenja metode remove() iteratora
		 */
		@Override
		public boolean hasNext() {
			if(iteratorModificationCount != modificationCount) {
				throw new ConcurrentModificationException("The hashtable has been drastically changed. This iterator cannot iterate anymore!");
			}
			return countElements < size;
		}
		/**
		 * @throws NoSuchElementException ako nema sljedećeg elementa
		 * @throws ConcurrentModificationException Ako se dodavalo ili oduzimalo elemente tablice raspršenog adresiranja bez korištenja metode remove() iteratora
		 */
		@Override
		public TableEntry<K, V> next() {
			if(!hasNext()) {
				throw new NoSuchElementException("No more elements to iterate over!");
			}
			
			if(currentEntry == null) {
				for(int i = currentIndex, limit = table.length; i < limit; i++) {
					currentEntry = table[i];
					if(!(currentEntry == null)) {
						currentIndex = i;
						countElements++;
						return currentEntry;
					}
				}
			} else if((currentEntry = currentEntry.next) == null) {
				currentIndex++;
				return next();
			}
			countElements++;
			return currentEntry;
		}
		
		/**
		 * @throws IllegalStateException ako nije pozvana metoda {@code next} prije prvog ili nakon bilo kojeg sljedećeg poziva metode {@code remove}
		 * @throws ConcurrentModificationException Ako se dodavalo ili oduzimalo elemente tablice raspršenog adresiranja bez korištenja metode remove() iteratora
		 */
		@Override
		public void remove() {
			if(iteratorModificationCount != modificationCount) {
				throw new ConcurrentModificationException("The hashtable has been drastically changed. This iterator cannot iterate anymore!");
			}
			if(currentEntry == null) {
				throw new IllegalStateException("Cannot remove if next method hasn't been invoked!");
			}
			if(!containsKey(currentEntry.key)) {
				throw new IllegalStateException("Cannot remove if next method hasn't been invoked!");
			}
			SimpleHashtable.this.remove(currentEntry.key);
			countElements--;
			iteratorModificationCount++;
		}
		
	}
	
	/**
	 * Tvornica iteratora nad tablicom raspršenog adresiranja.
	 * 
	 * @return Primjerak iteratora
	 */
	@Override
	public Iterator<SimpleHashtable.TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}
	
	/**
	 * Računa prvu vrijednost potencije 2 koja je jednaka ili veća danom broju.
	 * @param value 
	 * @return Prva potencija broja 2 veća ili jednaka argumentu
	 */
	private int getFirstPowerOf2EqualOrGreaterThan(int value) {
		int pow = 0, remainder = 0;
		while(value != 1) {
			if(value % 2 == 1) {
				remainder = 1;
			}
			value /= 2;
			pow++;
		}
		return (int) Math.pow(2, pow + remainder);
	}
}
