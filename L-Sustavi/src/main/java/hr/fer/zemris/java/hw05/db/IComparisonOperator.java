package hr.fer.zemris.java.hw05.db;
/**
 * Apstraktna strategija za uspoređivanje dva Stringa.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
@FunctionalInterface
public interface IComparisonOperator {
	/**
	 * Metoda koja se koristi za usporedbu Stringova po nekom kriteriju.
	 * 
	 * @param value1 String koji se ispituje
	 * @param value2 String koji se koristi za ispitivanje
	 * @return <code>true</code> ako prvi argument zadovoljava uvjet, <code>false</code> inače.
	 */
	boolean satisfied(String value1, String value2);
}
