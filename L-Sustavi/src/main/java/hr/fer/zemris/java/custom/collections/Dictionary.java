package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Parametrizirana klasa koja predstavlja mapu. Ključevi ne smiju biti null, dok vrijednosti smiju.
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 * @param <K> Tip ključa
 * @param <V> Tip vrijednosti
 */
public class Dictionary<K,V> {
	
	/**
	 * Kolekcija koja sadrži polje u koje se spremaju zapisi.
	 */
	private ArrayIndexedCollection<Entry<K,V>> dictionary;
	
	/**
	 * Parametrizirana klasa koja predstavlja jedan zapis u mapi.
	 * 
	 * @author Petar Kovač
	 * @version 1.0
	 *
	 * @param <E> Tip ključa
	 * @param <T> Tip vrijednosti
	 */
	private static class Entry<E,T> {
		/**
		 * Ključ kojom se dohvaća vrijednost.
		 */
		private E key;
		/**
		 * Vrijednost koja se veže uz neki ključ.
		 */
		private T value;
		
		/**
		 * Zadani konstruktor za zapis u mapi.
		 * 
		 * @param key Ključ
		 * @param value Vrijednost
		 * @throws NullPointerException ako je kao argument za key dan <code>null</code>
		 */
		public Entry(E key, T value) {
			Objects.requireNonNull(key);
			this.key = key;
			this.value = value;
		}

		/**
		 * Equals metoda uspoređuje pripadne ključeve dva zapisa equals metodom.
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Entry))
				return false;
			@SuppressWarnings("rawtypes")
			Entry other = (Entry) obj;
			return Objects.equals(key, other.key);
		}
		
		/**
		 * Postavlja vrijednost ovog zapisa.
		 * @param value Vrijednost
		 */
		public void setValue(T value) {
			this.value = value;
		}

		/**
		 * Dohvaća vrijednost ovog zapisa.
		 * @return T Vrijednost
		 */
		public T getValue() {
			return this.value;
		}
		
		/**
		 * Dohvaća vrijednost ključa ovog zapisa.
		 * @return E Ključ
		 */
		public E getKey() {
			return this.key;
		}
				
	}
	/**
	 * Zadani konstruktor za Dictionary. Stvara kolekciju veličine definirane razredom {@link ArrayIndexedCollection}.
	 */
	public Dictionary() {
		this.dictionary = new ArrayIndexedCollection<>();
	}
	
	/**
	 * Provjerava je li mapa prazna.
	 * @return <code>true</code> ako je mapa prazna, inače <code>false</code>
	 */
	public boolean isEmpty() {
		return dictionary.isEmpty();
	}
	
	/**
	 * Vraća broj zapisa u mapi.
	 * @return <code> int </code> broj zapisa u mapi
	 */
	public int size() {
		return dictionary.size();
	}
	/**
	 * Miče sve elemente iz mape.
	 */
	public void clear() {
		dictionary.clear();
	}
	
	/**
	 * Stavlja zapis na kraj mape, ako tog zapisa nema, inače ako zapis postoji postavlja novu vrijednost za zapis određen ključem key.
	 * 
	 * @param key Ključ
	 * @param value Vrijednost
	 * 
	 * @throws NullPointerException ako je vrijednost ključa jednaka <code>null</code>
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);
		int index;
		Entry<K, V> entry = new Entry<>(key, value);
		if((index = dictionary.indexOf(entry)) == -1) {
			dictionary.add(entry);
			return;
		}
		
		dictionary.get(index).setValue(value);
	}
	
	/**
	 * Dohvaća vrijednost koja je vezana za ovaj ključ.
	 * @param key Ključ
	 * @return Vrijednost ako zapis određen ključem u mapi, <code>null</code> inače.
	 * @throws NullPointerException ako je dan ključ jednak <code>null</code>
	 */
	public V get(Object key) {
		Objects.requireNonNull(key);
		Entry<K,V> entry;
		for(int i = 0, limit = dictionary.size(); i < limit; i++) {
			entry = dictionary.get(i);
			if(entry.getKey().equals(key)) {
				return entry.getValue();
			}
		}

		return null;
	}
}
