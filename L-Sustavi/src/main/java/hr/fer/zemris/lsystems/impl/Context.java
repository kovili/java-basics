package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * Klasa koja čuva kontekst jedne kornjače.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class Context {
	/**
	 * Stog na kojem se čuvaju stanja kornjače.
	 */
	ObjectStack<TurtleState> stack;
	
	/**
	 * Zadani konstruktor za Context.
	 */
	public Context() {
		stack = new ObjectStack<>();
	}
	/**
	 * Vraća jedno spremljeno stanje kornjače.
	 * @return {@link TurtleState} Stanje kornjače.
	 * @throws EmptyStackException Ako nema spremljenih stanja.
	 */
	public TurtleState getCurrentState() {
		return stack.peek();
	}
	/**
	 * Sprema trenutno stanje kornjače.
	 * @param state {@link TurtleState} Trenutno stanje kornjače.
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**
	 * Briše zadnje spremljeno stanje kornjače.
	 * @throws EmptyStackException Ako nema spremljenih stanja.
	 */
	public void popState() {
		stack.pop();
	}
	
}
