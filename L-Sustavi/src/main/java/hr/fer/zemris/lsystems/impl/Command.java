package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * Predstavlja jednu naredbu za kornjaču.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
@FunctionalInterface
public interface Command {
	
	/**
	 * Naredba koju će kornjača izvršiti.
	 * 
	 * @param ctx {@link Context} u kojem se pozvala naredba.
	 * @param painter {@link Painter} kojim se crta.
	 */
	void execute(Context ctx, Painter painter);
}
