package hr.fer.zemris.lsystems.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilderProvider;
import hr.fer.zemris.lsystems.gui.LSystemViewer;
import hr.fer.zemris.lsystems.impl.LSystemBuilderImpl;

/**
 *  Primjer koji pokazuje rad programa - vizualizacija Hilbertove krivulje.
 *  
 * @author Petar Kovač
 *
 */

public class HilbertCurveDemo {
	public static void main(String[] args) throws IOException {
		LSystemViewer.showLSystem(create(LSystemBuilderImpl::new));
		
	}
	
	private static LSystem create(LSystemBuilderProvider provider) throws IOException{
		List<String> lines = Files.readAllLines(
				 Paths.get("./src/main/resources/hilbertCurve.txt"),
				 StandardCharsets.UTF_8
				);
		String []data = new String[lines.size()];
		for(int i = 0; i < lines.size(); i++) {
			data[i] = lines.get(i);
		}

		return provider.createLSystemBuilder().configureFromText(data).build();
	}
}
