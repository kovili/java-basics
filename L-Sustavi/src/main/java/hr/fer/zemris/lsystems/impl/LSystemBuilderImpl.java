package hr.fer.zemris.lsystems.impl;

import java.awt.Color; 
import java.util.Objects;
import hr.fer.zemris.java.custom.collections.Dictionary;
import hr.fer.zemris.lsystem.impl.commands.*;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.math.Vector2D;
/**
 * Klasa koja gradi i modelira objekte koje je moguće konfigurirati.
 * Vraća jedan konkretan Lindermayerov sustav prema zadanoj konfiguraciji.
 *
 */
public class LSystemBuilderImpl implements LSystemBuilder{
	/**
	 * Maksimalna duljina heksadecimalnog argumenta za boju.
	 */
	private static final int MAX_RGB_HEX_LENGTH = 6;
	/**
	 * Zadana duljina kornjačinog koraka.
	 */
	private static final double DEFAULT_UNIT_LENGTH = 0.1;
	/**
	 * Zadani faktor uvećanja(smanjenja) kornjačinog koraka za sustav dubine depth.
	 */
	private static final double DEFAULT_UNIT_SCALER = 1;
	/**
	 * Zadani početni kut gledanja kornjače.
	 * Kornjača će gledati u desno.
	 */
	private static final double DEFAULT_ANGLE = 0;
	/**
	 * Zadani početni aksiom iz kojeg se može generirati niz.
	 */
	private static final String DEFAULT_AXIOM = "";
	/**
	 * Zadani početni vektor položaja kornjače.
	 * Kornjača će se nalaziti u donjem lijevom kutu prozora.
	 */
	private static final Vector2D DEFAULT_ORIGIN = new Vector2D(0,0);
	/**
	 * Rječnik koji za dani ključ pamti njegovu zamjenu tj. produkciju.
	 */
	private Dictionary<Character, String>  registeredProductions;
	/**
	 * Rječnik koji za dani ključ pamti koju naredbu {@link Command} treba napraviti.
	 */
	private Dictionary<Character, Command> registeredActions;
	/**
	 * Duljina kornjačinog koraka - ako je jednak 1, onda je korak jednak širini/visini prozora.
	 */
	private double unitLength;
	/**
	 * Faktor kojim se umanjuje kornjačin korak za danu dubinu depth.
	 */
	private double unitLengthDegreeScaler;
	/**
	 * Početni položaj kornjače na prozoru.
	 */
	private Vector2D origin;
	/**
	 * Početni kut pod kojim kornjača 'gleda'.
	 */
	private double angle;
	/**
	 * Početni niz znakova iz kojeg se generira novi niz.
	 */
	private String axiom;
	
	/**
	 * Dani konstruktor za graditelja LSustava.
	 * Postavlja sve vrijednosti na dane konstante.
	 */
	public LSystemBuilderImpl() {
		this.registeredProductions = new Dictionary<Character, String>();
		this.registeredActions = new Dictionary<Character, Command>();
		this.unitLength = DEFAULT_UNIT_LENGTH;
		this.unitLengthDegreeScaler = DEFAULT_UNIT_SCALER;
		this.origin = DEFAULT_ORIGIN.copy();
		this.angle = DEFAULT_ANGLE;
		this.axiom = DEFAULT_AXIOM;
	}
	
	
	/**
	 * Gradi jedan primjerak LSustava koji koristi konfiguraciju koja je zadana ovom primjerku razreda graditelja LSustava.
	 * 
	 * @return {@link LSystemImpl} Primjerak Lindermayerova sustava.
	 */
	@Override
	public LSystem build() {
		return new LSystemImpl();
	}
	/**
	 * Iz danog teksta uzima direktive i pomoću njih konfigurira graditelj LSustava.
	 * 
	 * @param directives Polje direktiva
	 * @return {@link LSystemBuilderImpl} Konfiguriran LSystemBuilder.
	 * @throws NullPointerException ako je dan argument jednak <code>null</code>.
	 */
	@Override
	public LSystemBuilder configureFromText(String[] directives) {
		Objects.requireNonNull(directives);
		for(String directiveString : directives) {
			getDirective(directiveString);
		}
		return this;
	}
	
	/**
	 * Metoda koja uzima jednu direktivu i određuje naredbu 
	 * ili konfiguraciju koja se mora izvršiti.
	 * Poziva metode koje određuju pojedinu direktivu.
	 * Ako ne može prepoznati direktivu ne radi ništa.
	 * Ako može prepoznati direktivu, ali je ona krivo zadana, javlja grešku.
	 * 
	 * @param directiveString Jedna direktiva.
	 * @throws IllegalArgumentException ako prepozna direktivu, ali argumenti direktive nisu dobro zadani.
	 */
	private void getDirective(String directiveString) {
		if(directiveString.isBlank()) {
			return;
		}
		String []parser = directiveString.strip().split("\\s+");
		String keyword = parser[0];
		int length = parser.length;
		
		if(length > 2) {
			int lastElementSize = parser[length - 1].length() + 1;
			int keyCharSize;
			int commandSize;
			if(checkChar(parser[length - 2]) && parser[length - 2] != "/") {
				keyCharSize = parser[length - 2].length() + 1;
				int newSubstringLength = directiveString.length() - lastElementSize - keyCharSize;
				Character keyChar = parser[length - 2].charAt(0);
				String lastElementString = parser[length - 1];
				if(keyword.equals("production")) {
					getDirective(directiveString.substring(0, newSubstringLength));
					registerProduction(keyChar, lastElementString);
				} 
				if(keyword.equals("command")) {
					getDirective(directiveString.substring(0, newSubstringLength));
					registerCommand(keyChar, lastElementString);
				}
				
			}  else if (checkChar(parser[length - 3])) {
				keyCharSize = parser[length - 3].length() + 1;
				commandSize = parser[length - 2].length() + 1;
				int newSubstringLength = directiveString.length() - lastElementSize - commandSize - keyCharSize;
				if(keyword.equals("command")) {
					getDirective(directiveString.substring(0, newSubstringLength));
					Character keyChar = parser[length - 3].charAt(0);
					String commandString = parser[length - 2].concat(" ").concat(parser[length - 1]);
					registerCommand(keyChar, commandString);
				}
			}
		}
		
		switch(length) {
			case 1:
				return;
			case 2:
				if(keyword.equals("angle")) {
					setAngle(extractNumber(parser[1]));
				} 
				if(keyword.equals("unitLength")) {
					setUnitLength(determineExpression(parser[1]));
				} 
				if(keyword.equals("axiom")) {
					setAxiom(parser[1]);
				}
				if(keyword.equals("unitLengthDegreeScaler")) {
					setUnitLengthDegreeScaler((determineExpression(parser[1])));
				}
				return;
			case 3:
				if(keyword.equals("unitLengthDegreeScaler")) {
					setUnitLengthDegreeScaler((determineExpression(parser[1].concat(parser[2]))));
				} 
				if(keyword.equals("origin")) {
					setOrigin(extractNumber(parser[1]), extractNumber(parser[2]));
				}
				return;
			case 4:
				if(keyword.equals("unitLengthDegreeScaler")) {
					setUnitLengthDegreeScaler((determineExpression(parser[1].concat(parser[2]).concat(parser[3]))));
				} 
		}
	}
	
	/**
	 * Provjerava je li nešto znak ili nije. Jedini uvjet je duljina danog argumenta.
	 * 
	 * @param check String koji se provjerava
	 * @return <code>true</code> ako je duljina Stringa 1, <code>false</code> inače.
	 */
	private boolean checkChar(String check) {
		return check.length() == 1;
	}
	
	/**
	 * Određuje izraz koji može biti ili decimalni broj ili dva decimalna brojeva odvojena znakom '/' npr. 2.6 /4
	 * U drugom slučaju se kao rezultat res dobiva: res = (a/b).
	 * 
	 * @param expression String iz kojeg se izvlači izraz.
	 * @return Broj koji se može izvući iz izraza.
	 * @throws IllegalArgumentException ako izraz nema znak '/' i barem jedan znak koji nije broj.
	 */
	private double determineExpression(String expression) {
		if(!expression.contains("/")) {
			return extractNumber(expression);
		} else {
			String []divisionElems = expression.split("\\s*/\\s*");
			return extractNumber(divisionElems[0]) / extractNumber(divisionElems[1]);
		}
	}
	/**
	 * Dodaje jednu naredbu, ako je ona definirana, u rječnik i veže je za dani ključ.
	 * 
	 * @param key Ključ naredbe
	 * @param commandString Tekstualni prikaz naredbe
	 * @return {@link LSystemBuilderImpl} Konfigurirani primjerak ovog razreda.
	 * @throws NullPointerException ako je za commandString dan <code>null</code>.
	 * 
	 */
	@Override
	public LSystemBuilder registerCommand(char key, String commandString) {
		Objects.requireNonNull(commandString);
		Command command = getCommand(commandString);
		if(registeredActions.get(key) == null) {
			registeredActions.put(key, command);
		}
		return this;
	}

	/**
	 * Dodaje jednu produkciju u rječnik i veže je za dani ključ.
	 * 
	 * @param axiom Ključ produkcije.
	 * @param production Tekstualni prikaz produkcije.
	 * @return {@link LSystemBuilderImpl} Konfigurirani primjerak ovog razreda.
	 * @throws NullPointerException ako je za production dan <code>null</code>.
	 * 
	 */
	@Override
	public LSystemBuilder registerProduction(char axiom, String production) {
		Objects.requireNonNull(production);
		if(registeredProductions.get(axiom) == null) {
			registeredProductions.put(axiom, production);
		}
		return this;
	}

	/**
	 * Postavlja kut gledanja kornjače na danu vrijednost u stupnjevima.
	 * 
	 * @param angle Kut pod kojim će kornjača gledati.
	 * @return {@link LSystemBuilderImpl} Konfigurirani primjerak ovog razreda.
	 */
	@Override
	public LSystemBuilder setAngle(double angle) {
		this.angle = angle;
		return this;
	}

	/**
	 * Postavlja početni niz na dani niz.
	 * 
	 * @param axiom {@link #axiom}.
	 * @return {@link LSystemBuilderImpl} Konfigurirani primjerak ovog razreda.
	 * @throws NullPointerException ako je za axiom dan <code>null</code>.
	 */
	@Override
	public LSystemBuilder setAxiom(String axiom) {
		Objects.requireNonNull(axiom);
		this.axiom = axiom;
		return this;
	}
	
	/**
	 * Konfigurira {@link #origin}, početni položaj kornjače, na dane argumente.
	 * 
	 * @param coorX Koordinata  x ∈[0, 1].
	 * @param coorY Koordinata  y∈[0, 1].
	 * @return {@link LSystemBuilderImpl} Konfigurirani primjerak ovog razreda.
	 * @throws IllegalArgumentException ako je dan argument koji nije ∈[0, 1].
	 * 
	 */
	@Override
	public LSystemBuilder setOrigin(double coorX, double coorY) {
		if((coorX > 1 || coorX < 0) || (coorY > 1 || coorY < 0)) {
			throw new IllegalArgumentException("Origin coordinates must be in range [0, 1]");
		}
		this.origin = new Vector2D(coorX, coorY);
		return this;
	}

	/**
	 * Konfigurira {@link #unitLength} kornjačin korak.
	 * 
	 * @param unitLength Novi kornjačin korak.
	 * @return {@link LSystemBuilderImpl} Konfigurirani primjerak ovog razreda.
	 */
	@Override
	public LSystemBuilder setUnitLength(double unitLength) {
		this.unitLength = unitLength;
		return this;
	}

	/**
	 * Konfigurira {@link #unitLengthDegreeScaler} faktor uvećanja kornjačina koraka.
	 * 
	 * @param unitLengthDegreeScaler Novi faktor uvećanja.
	 * @return {@link LSystemBuilderImpl} Konfigurirani primjerak ovog razreda.
	 */
	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double unitLengthDegreeScaler) {
		this.unitLengthDegreeScaler = unitLengthDegreeScaler;
		return this;
	}
	
	/**
	 * Iz danog String-a parsira i određuje naredbu. Ako se naredba može prepoznati stvara njen primjerak.
	 * Ako ne može, ne radi ništa.
	 * 
	 * @param commandString Tekstualni prikaz naredbe.
	 * @return {@link Command} Primjerak naredbe.
	 * @throws IllegalArgumentException Ako se naredba može prepoznati, ali su dani argumenti koji se ne mogu protumačiti.
	 */
	private Command getCommand(String commandString) {
		if(commandString.isBlank()) {
			return null;
		}
		String []parser = commandString.strip().split(" ");
		String keyword = parser[0];
		
		if(parser.length == 1) {
			if(keyword.equals("pop")) {
				return new PopCommand();
			} else if(keyword.equals("push")) {
				return new PushCommand();
			}
		}
				
		if(parser.length == 2) {
			if(keyword.equals("draw")) {
				return new DrawCommand(extractNumber(parser[1]));
			} else if(keyword.equals("rotate")) {
				return new RotateCommand(Math.toRadians(extractNumber(parser[1])));
			} else if(keyword.equals("skip")) {
				return new SkipCommand(extractNumber(parser[1]));
			} else if(keyword.equals("scale")) {
				return new ScaleCommand(extractNumber(parser[1]));
			} else if(keyword.equals("color")) {
				return new ColorCommand(extractColor(parser[1]));
			}
		}
		
		return null;
	}
	
	/**
	 * Iz danog Stringa izvlači decimalni broj, ako je to moguće.
	 * 
	 * @param parseable Tekstualni prikaz broja kojeg želimo parsirati.
	 * @return Parsirani broj.
	 * @throws IllegalArgumentException Ako je kao argument dano nešto što nije broj.
	 */
	private double extractNumber(String parseable) {
		try {
			return Double.parseDouble(parseable);
		} catch(NumberFormatException exc) {
			throw new IllegalArgumentException("Second command argument cannot be parsed to number!");
		}
	}
	/**
	 * Iz danog Stringa izvlači boju, ako je to moguće.
	 * 
	 * @param parseable RGB definirana boja koju želimo parsirati.
	 * @return {@link Color} Boja.
	 * @throws IllegalArgumentException Ako je kao argument dano nešto što nije RGB definirana boja.
	 */
	private Color extractColor(String colorString) {
		if(colorString.length() <= MAX_RGB_HEX_LENGTH) {
			try {
				return new Color(Math.abs(Integer.parseInt(colorString, 16)));
			} catch(NumberFormatException exc) {
				System.out.println("Second command argument cannot be parsed to RGB");
			}
		}
		throw new IllegalArgumentException("The command must contain only 1 argument of type hexadecimal number");
	}
	
	/**
	 * Implementacija jednog Lindermayerovog sustava. 
	 * Klasa čije se metode koriste za vizualizaciju Lindermayerovih sustava.
	 * 
	 * @author Petar Kovač
	 * @version 1.0
	 */
	private class LSystemImpl implements LSystem{
		/**
		 * Varijabla u kojoj se čuva originalni aksiom prije primjene produkcije,
		 * a novi niz nakon primjene produkcije.
		 */
		private String savedAxiom;
		
		/**
		 * Kornjačin kontekst ove implementacije LSistema.
		 */
		private Context ctx;
		/**
		 * Kornjačino početno stanje.
		 */
		private TurtleState savedState;
	
		/**
		 * Zadani konstruktor za LSystemImpl. Stvara novi {@link #ctx} i {@link #savedState}.
		 */
		public LSystemImpl() {
			this.ctx = new Context();
			this.savedState = new TurtleState(origin.copy(), new Vector2D(1,0).rotated(Math.toRadians(angle)), Color.BLACK, unitLength);
			this.savedAxiom = new String(axiom);
		}
		
		/**
		 * Metoda koja se poziva svaki put kada se mijenja dubina generacije fraktala
		 * Poziva se i kada se mijenja stanje prozora tj. kada se prozor pomiče ili kada mu se mijenja veličina.
		 *
		 * @param depth Dubina generacije fraktala
		 * @param Painter Objekt za crtanje linija
		 */
		@Override
		public void draw(int depth, Painter painter) {
			savedAxiom = new String(axiom);
			TurtleState state = savedState.copy();
			ctx.pushState(state);
			ScaleCommand scale = new ScaleCommand(Math.pow(unitLengthDegreeScaler, depth));
			scale.execute(ctx, painter);
			char []production = generate(depth).toCharArray();
			Command command;
			for(Character key : production) {
				if((command = registeredActions.get(key)) != null) {
					command.execute(ctx, painter);
				}
			}
			ctx.popState();
		}
			
			
		/**
		 * Metoda koja iz početnog aksioma generira završni niz nakon (depth - 1) produkcija.
		 * 
		 * @param depth Dubina generacije niza
		 * @return Završni niz
		 */
		@Override
		public String generate(int depth) {
			if(depth == 0) {
				return savedAxiom;
			}
			StringBuilder builder = new StringBuilder();
			char[] oldAxiom = savedAxiom.toCharArray();
			String production;
			for(Character key : oldAxiom) {
				if((production = registeredProductions.get(key)) != null) {
					builder.append(production);
				} else {
					builder.append(key);
				}
			}
			savedAxiom = builder.toString();
			return generate(depth - 1);
		}
		
	}

}
