package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.zemris.math.Vector2D;

/**
 * Klasa modelira stanje jedne kornjače (naravno, u smislu računalne grafike).
 * 
 * @author Petar Kovač
 * @version 1.0
 *
 */

public class TurtleState {
	/**
	 * Trenutna pozicija na ekranu, koordinate ovog vektora mogu biti samo ∈[0, 1].
	 */
	Vector2D position;
	/**
	 * Trenutni kut pod kojim je kornjača okrenuta, koordinate ovog vektora mogu biti samo ∈[0, 1].
	 * Ovo je radijusvektor pa modul ovog vektora mora biti jednak 1.
	 */
	Vector2D angle;
	/**
	 * Boja kojom će kornjača crtati.
	 * Definira boju putem RGB modela boje.
	 */
	Color color;
	/**
	 * Efektivna duljina osnovnog pomaka kornjača. Ovo je kornjačin korak.
	 * Vrijednosti ove varijable smiju biti samo ∈[0, 1].
	 */
	double effectiveLength;
	
	/**
	 * Zadani konstruktor za stanje kornjače.
	 * Postavlja kornjaču u donji desni ugao, usmjerava je desno i zadaje joj crnu boju.
	 * Zadana efektivna duljina je 1.
	 */
	public TurtleState() {
		this(new Vector2D(0, 0), new Vector2D(1, 0), Color.BLACK, 1);
	}
	
	/**
	 * Konstruktor za stanje kornjače.
	 * 
	 * @param position {@link #position}
	 * @param angle {@link #angle}
	 * @param color {@link #color}
	 * @param effectLength {@link #effectiveLength}
	 */
	public TurtleState(Vector2D position, Vector2D angle, Color color, double effectLength) {
		this.position = position;
		this.angle = angle;
		this.color = color;
		this.effectiveLength = effectLength;
	}


	/**
	 * Metoda daje kopiju trenutnog kornjačinog stanja.
	 * Mijenjanje kopije ne mijenja original, vrijedi i obrat.
	 * 
	 * @return {@link TurtleState} Trenutno stanje kornjače.
	 */
	public TurtleState copy() {
		return new TurtleState(position.copy(), angle.copy(), color, effectiveLength);
	}
	
	/**
	 * Vraća trenutnu poziciju kornjače
	 * @return {@link #position}
	 */
	public Vector2D getPosition() {
		return position;
	}

	/**
	 * Vraća radijuvektor u čijem smjeru kornjača gleda.
	 * @return {@link #angle}
	 */
	public Vector2D getAngle() {
		return angle;
	}

	/**
	 * Vraća trenutnu boju kojom kornjača crta.
	 * @return {@link #color}
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Postavlja boju kojom kornjača crta.
	 * @param color {@link #color}
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Vraća kornjačin korak.
	 * @return {@link #effectiveLength}
	 */
	public double getEffectiveLength() {
		return effectiveLength;
	}
	
	/**
	 * Postavlja duljinu kornjačinog koraka.
	 * @param effectiveLength {@link #effectiveLength}
	 */
	public void setEffectiveLength(double effectiveLength) {
		this.effectiveLength = effectiveLength;
	}

	
}
