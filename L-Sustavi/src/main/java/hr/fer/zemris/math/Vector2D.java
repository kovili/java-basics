package hr.fer.zemris.math;

import java.util.Objects;

/**
 * Klasa predstavlja jednostavan 2D vektor.
 * 
 * @author Petar Kovač
 * @version 1.0
 */
public class Vector2D {
	/**
	 * Koordinata x vektora.
	 */
	private double x;
	/**
	 * Koordinata y vektora.
	 */
	private double y;
	
	/**
	 * Zadani konstruktor za 2D vektor.
	 * 
	 * @param x Koordinata x
	 * @param y Koordinata y
	 */
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Translatira koordinate vektora nekim drugim vektorom.
	 * 
	 * @param offset Drugi vektor
	 * @throws NullPointerException ako je kao argument predan <code>null</code>
	 */
	public void translate(Vector2D offset) {
		Objects.requireNonNull(offset);
		this.x += offset.getX();
		this.y += offset.getY();
	}
	
	/**
	 * Vraća novi vektor - rezultat translatiranja ovog vektora nekim drugim vektorom.
	 * 
	 * @param offset Drugi vektor
	 * @return {@link Vector2D}
	 */
	public Vector2D translated(Vector2D offset) {
		Vector2D vector = new Vector2D(this.x, this.y);
		vector.translate(offset);
		return vector;
	}
	
	/**
	 * Rotira vektor za dani kut.
	 * 
	 * @param angle Kut rotacije u radijanima
	 */
	public void rotate(double angle) {
		double oldX = this.x, oldY = this.y;
		this.x = oldX * Math.cos(angle) - oldY * Math.sin(angle);
		this.y = oldX * Math.sin(angle) + oldY * Math.cos(angle);
	}
	
	/**
	 * Vraća novi vektor - rezultat rotiranja ovog vektora za neki kut.
	 * 
	 * @param angle Kut rotacije u radijanima
	 * @return {@link Vector2D}
	 */
	public Vector2D rotated(double angle) {
		Vector2D vector = new Vector2D(this.x, this.y);
		vector.rotate(angle);
		return vector;
	}
	/**
	 * Skalira vektor za faktor scaler.
	 * 
	 * @param scaler Faktor uvećanja
	 */
	public void scale(double scaler) {
		this.x *= scaler;
		this.y *= scaler;
	}
	
	/**
	 * Vraća novi vektor - rezultat skaliranja ovog vektora danim faktorom uvećanja.
	 * 
	 * @param scaler Faktor uvećanja
	 * @return {@link Vector2D}
	 */
	public Vector2D scaled(double scaler) {
		Vector2D vector = new Vector2D(this.x, this.y);
		vector.scale(scaler);
		return vector;
	}
	
	/**
	 * Vraća novi vektor - kopiju ovog vektora.
	 * 
	 * @return {@link Vector2D}
	 */
	public Vector2D copy() {
		return new Vector2D(this.x, this.y);
	}
	
	/**
	 * Vraća koordinatu x.
	 * 
	 * @return Koordinata x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Vraća koordinatu y.
	 * 
	 * @return Koordinata y
	 */
	public double getY() {
		return y;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vector2D))
			return false;
		Vector2D other = (Vector2D) obj;
		return Double.doubleToLongBits(x) == Double.doubleToLongBits(other.x)
				&& Double.doubleToLongBits(y) == Double.doubleToLongBits(other.y);
	}
	
	
	
	
}
