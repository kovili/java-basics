package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw05.db.parser.QueryFilter;

class QueryFilterTest {

	@Test
	void nullPointerTest() {
		assertThrows(NullPointerException.class, () -> new QueryFilter(null));
	}
	
	@Test
	void testPass() {
		List<ConditionalExpression> filter = new ArrayList<>();
		filter.add(new ConditionalExpression
				(FieldValueGetters.JMBAG, 
				"00", ComparisonOperators.EQUALS));
		
		filter.add(new ConditionalExpression
			(FieldValueGetters.LAST_NAME, 
			"Ko*", ComparisonOperators.LIKE));
		
		filter.add(new ConditionalExpression
			(FieldValueGetters.FIRST_NAME, 
			"*ar", ComparisonOperators.LIKE));
		
		StudentRecord record = new StudentRecord("Petar", "Kovač", "00", -1);
		
		QueryFilter tester = new QueryFilter(filter);
		assertTrue(tester.accepts(record));
	}
	
	@Test
	void testFail() {
		List<ConditionalExpression> filter = new ArrayList<>();
		filter.add(new ConditionalExpression
				(FieldValueGetters.JMBAG, 
				"00", ComparisonOperators.EQUALS));
		
		filter.add(new ConditionalExpression
			(FieldValueGetters.LAST_NAME, 
			"Ko*", ComparisonOperators.LIKE));
		
		filter.add(new ConditionalExpression
			(FieldValueGetters.FIRST_NAME, 
			"*ar", ComparisonOperators.LIKE));
		QueryFilter tester = new QueryFilter(filter);
		
		StudentRecord record = new StudentRecord("Petar", "Malk", "00", -1);
		
		assertFalse(tester.accepts(record));
		
		record = new StudentRecord("r", "Kovač", "00", -1);
		
		assertFalse(tester.accepts(record));
		
		record = new StudentRecord("Petar", "Kovač", "2130", -1);
		
		assertFalse(tester.accepts(record));
	}

}
