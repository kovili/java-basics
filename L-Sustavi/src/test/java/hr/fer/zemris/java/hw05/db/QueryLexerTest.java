package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*; 

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw05.db.parser.LexerException;
import hr.fer.zemris.java.hw05.db.parser.QueryLexer;
import hr.fer.zemris.java.hw05.db.parser.QueryToken;
import hr.fer.zemris.java.hw05.db.parser.QueryTokenType;

class QueryLexerTest {

	QueryLexer lexer;
	
	@Test
	void nullPointerTest() {
		assertThrows(NullPointerException.class, () -> new QueryLexer(null));
	}
	
	@Test
	void emptyLineTest() {
		lexer = new QueryLexer(" \t \n    \t");
		QueryToken token = lexer.nextToken();
		assertEquals(token.getType(), QueryTokenType.EOF);
	}
	
	@Test
	void invalidTextTest() {
		assertThrows(LexerException.class, () -> new QueryLexer("not actual good text").nextToken());
	}
	//Bez parsera ostali testovi nisu mogući

}
