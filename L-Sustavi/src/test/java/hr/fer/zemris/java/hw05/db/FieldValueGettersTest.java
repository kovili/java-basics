package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

class FieldValueGettersTest {
	StudentDatabase database = loader();
	StudentRecord []records = recordLoader();
	
	@Test
	void firstNameGetterTest() {
		IFieldValueGetter getter = FieldValueGetters.FIRST_NAME;
		String name;
		
		name = getter.get(records[1]);
		assertTrue(name.equals("Marin"));
		
		name = getter.get(records[7]);
		assertTrue(name.equals("Sanjin"));
		
		name = getter.get(records[14]);
		assertTrue(name.equals("Mirta"));
		
		name = getter.get(records[21]);
		assertTrue(name.equals("Antonija"));
		
		name = getter.get(records[28]);
		assertTrue(name.equals("Nenad"));
		
		name = getter.get(records[35]);
		assertTrue(name.equals("Ivan"));
		
		name = getter.get(records[42]);
		assertTrue(name.equals("Nikola"));
		
		name = getter.get(records[49]);
		assertTrue(name.equals("Branimir"));
		
		name = getter.get(records[56]);
		assertTrue(name.equals("Veljko"));
		
		name = getter.get(records[63]);
		assertTrue(name.equals("Željko"));
		
		assertFalse(name.equals("željko"));
	}
	
	@Test
	void lastNameGetterTest() {
		IFieldValueGetter getter = FieldValueGetters.LAST_NAME;
		String name;
		
		name = getter.get(records[1]);
		assertTrue(name.equals("Akšamović"));
		
		name = getter.get(records[7]);
		assertTrue(name.equals("Čima"));
		
		name = getter.get(records[14]);
		assertTrue(name.equals("Gašić"));
		
		name = getter.get(records[21]);
		assertTrue(name.equals("Jakobušić"));
		
		name = getter.get(records[28]);
		assertTrue(name.equals("Kosanović"));
		
		name = getter.get(records[35]);
		assertTrue(name.equals("Marić"));
		
		name = getter.get(records[42]);
		assertTrue(name.equals("Palajić"));
		
		name = getter.get(records[49]);
		assertTrue(name.equals("Saratlija"));
		
		name = getter.get(records[56]);
		assertTrue(name.equals("Šimunović"));
		
		name = getter.get(records[63]);
		assertTrue(name.equals("Žabčić"));
		
		assertFalse(name.equals("*abčić"));
	}
	
	@Test
	void jmbagGetterTest() {
		IFieldValueGetter getter = FieldValueGetters.JMBAG;
		String jmbag;
		
		jmbag = getter.get(records[4]);
		assertTrue(jmbag.equals("0000000004"));
		jmbag = getter.get(records[6]);
		assertTrue(jmbag.equals("0000000006"));
		jmbag = getter.get(records[13]);
		assertTrue(jmbag.equals("0000000013"));
		jmbag = getter.get(records[23]);
		assertTrue(jmbag.equals("0000000023"));
		jmbag = getter.get(records[40]);
		assertTrue(jmbag.equals("0000000040"));
		jmbag = getter.get(records[58]);
		assertTrue(jmbag.equals("0000000058"));
		
		assertFalse(jmbag.equals("58"));
	}
	
	
	
	
	
	
	private static StudentDatabase loader(){
		try {
			List<String> lines = Files.readAllLines(
					Paths.get("./src/test/resources/database.txt"),
					StandardCharsets.UTF_8
					);
			String []data = new String[lines.size()];
			for(int i = 0; i < lines.size(); i++) {
				data[i] = lines.get(i);
			}
			return new StudentDatabase(data);
		} catch (IOException e) {
			System.err.println("Fatal error!");
			System.exit(0);
		}
		return null;

	}
	
	private StudentRecord[] recordLoader() {
		String jmbag = "000000000";
		StudentRecord []records = new StudentRecord[64];
		for(int i = 1; i < 10; i++) {
			records[i] = database.forJMBAG(jmbag + i);
		}
		jmbag = "00000000";
		for(int i = 10; i < 64; i++) {
			records[i] = database.forJMBAG(jmbag + i);
		}
		return records;
	}

}
