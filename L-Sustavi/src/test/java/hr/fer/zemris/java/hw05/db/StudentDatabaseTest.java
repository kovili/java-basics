package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*; 

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

class StudentDatabaseTest {
	
	StudentDatabase database = loader();
	StudentRecord []records = recordLoader();

	@Test
	void testGet() {

		assertTrue(database.forJMBAG("007") == null);
		assertTrue(database.forJMBAG("0000000000") == null);
		assertTrue(database.forJMBAG("0000000100") == null);
		
		assertTrue(records[1].getFirstName().equals("Marin"));
		assertTrue(records[1].getLastName().equals("Akšamović"));
		assertTrue(records[1].getJmbag().equals("0000000001"));
		
		assertTrue(records[15].getFirstName().equals("Kristijan"));
		assertTrue(records[15].getLastName().equals("Glavinić Pecotić"));
		assertTrue(records[15].getJmbag().equals("0000000015"));
		
		assertTrue(records[29].getFirstName().equals("Ivo"));
		assertTrue(records[29].getLastName().equals("Kos-Grabar"));
		assertTrue(records[29].getJmbag().equals("0000000029"));
		
		assertTrue(records[31].getFirstName().equals("Bojan"));
		assertTrue(records[31].getLastName().equals("Krušelj Posavec"));
		assertTrue(records[31].getJmbag().equals("0000000031"));
		
		assertTrue(records[63].getFirstName().equals("Željko"));
		assertTrue(records[63].getLastName().equals("Žabčić"));
		assertTrue(records[63].getJmbag().equals("0000000063"));
	}
	
	@Test
	void testFilter() {
		List<StudentRecord> filterList;
		filterList = database.filter(new IFilterFalse());
		assertEquals(0, filterList.size());
		filterList = database.filter(new IFilterTrue());
		assertEquals(63, filterList.size());
		filterList = database.filter(new IFilterFalse());
		assertEquals(0, filterList.size());
	}
	
	
	
	private static StudentDatabase loader(){
		try {
			List<String> lines = Files.readAllLines(
					Paths.get("./src/test/resources/database.txt"),
					StandardCharsets.UTF_8
					);
			String []data = new String[lines.size()];
			for(int i = 0; i < lines.size(); i++) {
				data[i] = lines.get(i);
			}
			return new StudentDatabase(data);
		} catch (IOException e) {
			System.err.println("Fatal error!");
			System.exit(0);
		}
		return null;

	}
	
	private StudentRecord[] recordLoader() {
		String jmbag = "000000000";
		StudentRecord []records = new StudentRecord[64];
		for(int i = 1; i < 10; i++) {
			records[i] = database.forJMBAG(jmbag + i);
		}
		jmbag = "00000000";
		for(int i = 10; i < 64; i++) {
			records[i] = database.forJMBAG(jmbag + i);
		}
		return records;
	}
	
	private static class IFilterFalse implements IFilter {

		public IFilterFalse() {}

		@Override
		public boolean accepts(StudentRecord record) {
			return false;
		}	
	}
	
	private static class IFilterTrue implements IFilter {

		public IFilterTrue() {}

		@Override
		public boolean accepts(StudentRecord record) {
			return true;
		}
		
	}

}
