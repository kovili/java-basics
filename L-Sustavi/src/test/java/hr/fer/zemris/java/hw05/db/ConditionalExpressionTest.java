package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

class ConditionalExpressionTest {
	StudentDatabase database = loader();
	StudentRecord []records = recordLoader();
	
	
	@Test
	void conditionalExpressionTest1() {
		ConditionalExpression expr = new ConditionalExpression(
				 FieldValueGetters.LAST_NAME,
				 "Bos*",
				 ComparisonOperators.LIKE
				);
		StudentRecord record = records[3];
		assertTrue(expr.getComparisonOperator().satisfied(
				expr.getFieldGetter().get(record),
				expr.getStringLiteral()));
		for(int i = 10; i < 60; i++) {
			record = records[i];
			assertFalse(expr.getComparisonOperator().satisfied(
					expr.getFieldGetter().get(record),
					expr.getStringLiteral()));
		}
	}
	
	@Test
	void conditionalExpressionTest2() {
		ConditionalExpression expr = new ConditionalExpression(
				 FieldValueGetters.LAST_NAME,
				 "*",
				 ComparisonOperators.LIKE
				);
		StudentRecord record = records[3];
		for(int i = 1; i < 64; i++) {
			record = records[i];
			assertTrue(expr.getComparisonOperator().satisfied(
					expr.getFieldGetter().get(record),
					expr.getStringLiteral()));
		}
		
		expr = new ConditionalExpression(
				 FieldValueGetters.LAST_NAME,
				 "Fran",
				 ComparisonOperators.LESS
				);
		for(int i = 1; i < 6; i++) {
			record = records[i];
			assertTrue(expr.getComparisonOperator().satisfied(
					expr.getFieldGetter().get(record),
					expr.getStringLiteral()));
		}
		for(int i = 12; i < 64; i++) {
			record = records[i];
			assertFalse(expr.getComparisonOperator().satisfied(
					expr.getFieldGetter().get(record),
					expr.getStringLiteral()));
		}
	}
	
	@Test
	void conditionalExpressionTest3() {
		ConditionalExpression expr = new ConditionalExpression(
				 FieldValueGetters.LAST_NAME,
				 "Kovač",
				 ComparisonOperators.EQUALS
				);
		StudentRecord record;
		for(int i = 1; i < 64; i++) {
			record = records[i];
			assertFalse(expr.getComparisonOperator().satisfied(
					expr.getFieldGetter().get(record),
					expr.getStringLiteral()));
		}
	}
	
	private static StudentDatabase loader(){
		try {
			List<String> lines = Files.readAllLines(
					Paths.get("./src/test/resources/database.txt"),
					StandardCharsets.UTF_8
					);
			String []data = new String[lines.size()];
			for(int i = 0; i < lines.size(); i++) {
				data[i] = lines.get(i);
			}
			return new StudentDatabase(data);
		} catch (IOException e) {
			System.err.println("Fatal error!");
			System.exit(0);
		}
		return null;

	}
	
	private StudentRecord[] recordLoader() {
		String jmbag = "000000000";
		StudentRecord []records = new StudentRecord[64];
		for(int i = 1; i < 10; i++) {
			records[i] = database.forJMBAG(jmbag + i);
		}
		jmbag = "00000000";
		for(int i = 10; i < 64; i++) {
			records[i] = database.forJMBAG(jmbag + i);
		}
		return records;
	}

}
