package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw05.db.parser.ParserException;
import hr.fer.zemris.java.hw05.db.parser.QueryParser;

class QueryParserTest {

	QueryParser parser;

	@Test
	void nullPointerTest() {
		assertThrows(NullPointerException.class, () -> new QueryParser(null));
	}
	
	@Test
	void emptyLineTest() {
		assertThrows(ParserException.class, () -> new QueryParser(""));
	}
	
	@Test
	void emptierLineTest() {
		assertThrows(ParserException.class, () -> new QueryParser("  \n \t"));
	}
	
	@Test
	void someTextTest() {
		assertThrows(ParserException.class, () -> new QueryParser("actually some text"));
	}
	
	@Test
	void incorrectQueryTestOnlyKeyword() {
		assertThrows(ParserException.class, () -> new QueryParser("  jmbag"));
	}
	
	@Test
	void incorrectQueryReverseOrder() {
		assertThrows(ParserException.class, () -> new QueryParser("\"0000000003\" = jmbag "));
	}
	
	@Test
	void incorrectQueryLikeNotCaps() {
		assertThrows(ParserException.class, () -> new QueryParser("jmbag LiKE \"00\""));
	}
	
	@Test
	void incorrectQueryMultipleOperators() {
		assertThrows(ParserException.class, () -> new QueryParser("jmbag == \"00\" "));
		assertThrows(ParserException.class, () -> new QueryParser("jmbag =! \"00\" "));
		assertThrows(ParserException.class, () -> new QueryParser("jmbag << \"00\" "));
		assertThrows(ParserException.class, () -> new QueryParser("jmbag <> \"00\" "));
		assertThrows(ParserException.class, () -> new QueryParser("jmbag >> \"00\" "));
		assertThrows(ParserException.class, () -> new QueryParser("jmbag LIKE < \"00\" "));
		assertThrows(ParserException.class, () -> new QueryParser("jmbag !< \"00\" "));
		assertThrows(ParserException.class, () -> new QueryParser("jmbag = LIKE \"00\" "));
	}
	
	@Test
	void correctQueryTest() {
		QueryParser parser = new QueryParser("jmbag=\"00\"");
		ConditionalExpression exp = new ConditionalExpression
									(FieldValueGetters.JMBAG, 
									"00", ComparisonOperators.EQUALS);
		List<ConditionalExpression> parserExpList = parser.getQuery();
		ConditionalExpression parserExp = parserExpList.get(0);
		assertEquals(exp.getComparisonOperator(), parserExp.getComparisonOperator());
		assertEquals(exp.getFieldGetter(), parserExp.getFieldGetter());
		assertEquals(exp.getStringLiteral(), parserExp.getStringLiteral());
	}
	
	@Test
	void longCorrectQueryTest() {
		QueryParser parser = new QueryParser("jmbag=\"00\"   anD   firstName LIKE \"Ko*\" AND lastName<=\"AAA\" and  firstName LIKE \"like\"       		");
		List<ConditionalExpression> parserExpList = parser.getQuery();
		ConditionalExpression exp = new ConditionalExpression
									(FieldValueGetters.JMBAG, 
									"00", ComparisonOperators.EQUALS);
		ConditionalExpression parserExp = parserExpList.get(0);
		assertEquals(exp.getComparisonOperator(), parserExp.getComparisonOperator());
		assertEquals(exp.getFieldGetter(), parserExp.getFieldGetter());
		assertEquals(exp.getStringLiteral(), parserExp.getStringLiteral());
		
		
		exp = new ConditionalExpression
			(FieldValueGetters.FIRST_NAME, 
			"Ko*", ComparisonOperators.LIKE);
		parserExp = parserExpList.get(1);
		assertEquals(exp.getComparisonOperator(), parserExp.getComparisonOperator());
		assertEquals(exp.getFieldGetter(), parserExp.getFieldGetter());
		assertEquals(exp.getStringLiteral(), parserExp.getStringLiteral());
		
		exp = new ConditionalExpression
			 (FieldValueGetters.LAST_NAME, 
			 "AAA", ComparisonOperators.LESS_OR_EQUALS);
		parserExp = parserExpList.get(2);
		assertEquals(exp.getComparisonOperator(), parserExp.getComparisonOperator());
		assertEquals(exp.getFieldGetter(), parserExp.getFieldGetter());
		assertEquals(exp.getStringLiteral(), parserExp.getStringLiteral());
		
		exp = new ConditionalExpression
			  (FieldValueGetters.FIRST_NAME, 
			  "like", ComparisonOperators.LIKE);
		parserExp = parserExpList.get(3);
		assertEquals(exp.getComparisonOperator(), parserExp.getComparisonOperator());
		assertEquals(exp.getFieldGetter(), parserExp.getFieldGetter());
		assertEquals(exp.getStringLiteral(), parserExp.getStringLiteral());
	}
	
	@Test
	void notDirectQueryTest() {
		assertThrows(IllegalStateException.class, () -> new QueryParser("jmbag=\"00\"   anD   firstName LIKE \"Ko*\"").getQueriedJMBAG());
	}
	
	@Test
	void directQueryTest() {
		QueryParser parser = new QueryParser("jmbag=\"00\"");
		assertTrue(parser.isDirectQuery());
		assertEquals("00", parser.getQueriedJMBAG());
	}
	
	

}
