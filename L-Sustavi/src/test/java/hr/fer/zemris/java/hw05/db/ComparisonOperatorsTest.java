package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*; 

import org.junit.jupiter.api.Test;

class ComparisonOperatorsTest {

	String ivan = "Ivan";
	String žlovimir = "Žlovimir";
	String andrea = "Andrea";
	String aaa = "AAA";
	String kos = "Kos-Grabar";
	String hrvatko = "Hrvatko";
	String andreja = "Andreja";
	String hugo = "Hugo";
	String bilotko = "Bilotko";
	
	String jmbag1 = "0000000036";
	String jmbag2 = "0000000054";
	String jmbag3 = "0000000072";
	String jmbag4 = "0000000800";
	String jmbag5 = "1234567890";
	
	@Test
	void lessTest() {
		IComparisonOperator filter = ComparisonOperators.LESS;
		assertTrue(filter.satisfied(ivan, žlovimir));
		assertFalse(filter.satisfied(ivan, andrea));
		assertTrue(filter.satisfied(andrea, andreja));
		assertFalse(filter.satisfied(hugo, hrvatko));
		
		assertTrue(filter.satisfied(jmbag1, jmbag2));
		assertFalse(filter.satisfied(jmbag5, jmbag4));
		assertTrue(filter.satisfied(jmbag3, jmbag5));
		assertFalse(filter.satisfied(jmbag3, jmbag1));
	}
	
	@Test
	void lessOrEqualsTest() {
		IComparisonOperator filter = ComparisonOperators.LESS_OR_EQUALS;
		assertTrue(filter.satisfied(andrea, andrea));
		assertFalse(filter.satisfied(ivan, andrea));
		assertTrue(filter.satisfied(andreja, andreja));
		assertFalse(filter.satisfied(hugo, hrvatko));
		
		assertTrue(filter.satisfied(jmbag1, jmbag1));
		assertFalse(filter.satisfied(jmbag5, jmbag4));
		assertTrue(filter.satisfied(jmbag3, jmbag3));
		assertFalse(filter.satisfied(jmbag3, jmbag1));
	}
	
	@Test
	void greaterTest() {
		IComparisonOperator filter = ComparisonOperators.GREATER;
		assertTrue(filter.satisfied(žlovimir, bilotko));
		assertFalse(filter.satisfied(andrea, andreja));
		assertTrue(filter.satisfied(hrvatko, aaa));
		assertFalse(filter.satisfied(ivan, žlovimir));
		
		assertTrue(filter.satisfied(jmbag5, jmbag4));
		assertFalse(filter.satisfied(jmbag1, jmbag3));
		assertTrue(filter.satisfied(jmbag2, jmbag1));
		assertFalse(filter.satisfied(jmbag3, jmbag5));
	}
	
	@Test
	void greaterOrEqualsTest() {
		IComparisonOperator filter = ComparisonOperators.GREATER_OR_EQUALS;
		assertTrue(filter.satisfied(žlovimir, bilotko));
		assertFalse(filter.satisfied(andrea, andreja));
		assertTrue(filter.satisfied(hrvatko, hrvatko));
		assertFalse(filter.satisfied(ivan, žlovimir));
		
		assertTrue(filter.satisfied(jmbag5, jmbag4));
		assertFalse(filter.satisfied(jmbag1, jmbag3));
		assertTrue(filter.satisfied(jmbag1, jmbag1));
		assertFalse(filter.satisfied(jmbag3, jmbag5));
	}
	
	@Test
	void equalsTest() {
		IComparisonOperator filter = ComparisonOperators.EQUALS;
		assertTrue(filter.satisfied(bilotko, bilotko));
		assertFalse(filter.satisfied(andrea, andreja));
		assertTrue(filter.satisfied(ivan, ivan));
		assertFalse(filter.satisfied(kos, aaa));
		
		assertTrue(filter.satisfied(jmbag3, jmbag3));
		assertFalse(filter.satisfied(jmbag4, jmbag3));
		assertTrue(filter.satisfied(jmbag2, jmbag2));
		assertFalse(filter.satisfied(jmbag3, jmbag2));
	}
	
	@Test
	void notEqualsTest() {
		IComparisonOperator filter = ComparisonOperators.NOT_EQUALS;
		assertTrue(filter.satisfied(žlovimir, bilotko));
		assertFalse(filter.satisfied(kos, kos));
		assertTrue(filter.satisfied(ivan, aaa));
		assertFalse(filter.satisfied(aaa, aaa));
		
		assertTrue(filter.satisfied(jmbag3, jmbag1));
		assertFalse(filter.satisfied(jmbag1, jmbag1));
		assertTrue(filter.satisfied(jmbag2, jmbag5));
		assertFalse(filter.satisfied(jmbag4, jmbag4));
	}
	
	@Test
	void testLike() {
		IComparisonOperator filter = ComparisonOperators.LIKE;
		assertTrue(filter.satisfied(žlovimir, "Ž*r"));
		assertTrue(filter.satisfied(žlovimir, "*"));
		assertTrue(filter.satisfied(žlovimir, "Ž*"));
		assertTrue(filter.satisfied(žlovimir, "*lovimir"));
		assertTrue(filter.satisfied(žlovimir, "Ž*lovimir"));
		assertTrue(filter.satisfied(žlovimir, "Ž*mir"));
		assertFalse(filter.satisfied(aaa, "AA*AA"));
		assertFalse(filter.satisfied("Zagreb", "Aba*"));
		assertTrue(filter.satisfied("AAAA", "AA*AA"));
		assertTrue(filter.satisfied("Krušelj Posavec", "*"));
		assertTrue(filter.satisfied("Krušelj Posavec", "K*savec"));
		assertTrue(filter.satisfied("Krušelj Posavec", "Krušelj P*savec"));
	}


}
