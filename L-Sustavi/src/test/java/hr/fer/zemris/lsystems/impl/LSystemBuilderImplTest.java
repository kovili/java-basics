package hr.fer.zemris.lsystems.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;

class LSystemBuilderImplTest {
	
	LSystemBuilder builder = new LSystemBuilderImpl().setAxiom("F").registerProduction('F', "F+F--F+F");
	LSystem system = builder.build();
	
	@Test
	void generate1Test() {
		assertEquals("F", system.generate(0));
		assertEquals("F+F--F+F", system.generate(1));
	}
	
	@Test
	void generate2Test() {
		assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", system.generate(2));
	}

}
